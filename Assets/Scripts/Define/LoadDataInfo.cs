﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class LoadDataInfo : IClearStruct
{
	public string AudioPath;
	public string WordDataPath;
	public string LyricsDataPath;
	public string CommandDataPath;

	public PlaySettingInfo PlaySetting;
	public StartPanelInfo StartInfo;
	public EndPanelInfo EndInfo;

	[System.Serializable]
	public class PlaySettingInfo
	{
		public bool ShowStartInfo;
		public bool AudioRepeat;
		public bool ShowTimeText;
		public bool ShowHiragana;
		public bool ShowLyricsEffect;
		public bool ShowLanguageLyrics;
		public float StartTime;
		public float EndTime;
	}

	[System.Serializable]
	public class StartPanelInfo
	{
		public string SongName;
		public string CVName;
		public string Info;
	}

	[System.Serializable]
	public class EndPanelInfo
	{
		public string Word;
		public string Info;
	}

	public void SortData(string relativePath)
	{
		AudioPath = ReplacePath(AudioPath, relativePath);
		WordDataPath = ReplacePath(WordDataPath, relativePath);
		LyricsDataPath = ReplacePath(LyricsDataPath, relativePath);
		CommandDataPath = ReplacePath(CommandDataPath, relativePath);
	}

	string ReplacePath(string pathString, string relativePath)
	{
		return Method.ParseToRightDownloadPath( pathString.Replace("/.../", Method.CombineString(relativePath, "/")) );
	}

	public void Clear()
	{
		StartInfo = null;
		EndInfo = null;
	}
}
