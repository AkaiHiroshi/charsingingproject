﻿using UnityEngine;
using System.Collections;

public class PanelUICtrllerBase : MonoBehaviour
{
	public virtual void SetActive(bool active)
	{
		gameObject.SetActive(active);
	}
}
