﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneManager : MonoBehaviour, IDownloadComplete
{
	static SceneManager gIns;
	public static SceneManager ins {
		get  {
			if (gIns == null)
				gIns = Method.GetInstance<SceneManager>();
			return gIns;
		}
	}

	public bool IgnorePlaySetting = false;
	public bool ShowStartInfo = true;
	public float FadeDuration = 1;
	public float ShowTitleDuration = 3;
	public string LoadDataDocumentURL;
	public string LoadDataDocumentName = "Data";
	public TextAsset LoadDataDocument;
	public GameObject LoadingPnael;
	public UnityEngine.UI.Text DebugText;
	public StartPanelUICtrller StartPnael;
	public EndPanelUICtrller EndPnael;
	public MsgPanelUICtrller MsgPnael;
	public LoadDataInfo LoadData;

	string RelativePath
	{
		get
		{
		#if UNITY_STANDALONE && !UNITY_EDITOR
			return Application.dataPath;
		#else
			return Application.dataPath;
		#endif
		}
	}

	public void ShowEndBoard()
	{
		SimpleFadeTool.FadeIn(0.1f);
		EndPnael.SetActive(true);
	}

	public void RepeatReset()
	{
		if (SimpleFadeTool.Alpha == 1)
			SimpleFadeTool.Alpha = 0;
		
		StartPnael.SetActive(false);
		EndPnael.SetActive(false);

		ExpressionAnimationCtrller[] script = FindObjectsOfType<ExpressionAnimationCtrller>();
		for (int i = 0, max = script.Length; i < max; i++)
		{
			if (script[i] != null)
				script[i].PlayAnimation("Default", false, true);
		}
	}

	IEnumerator Start()
	{
		string errorInfo = "";

		//Loading
		{
			MsgPnael.SetActive(false);
			LoadingPnael.SetActive(true);

			if (LoadDataDocument == null)
			{
				if (string.IsNullOrEmpty(LoadDataDocumentURL))
				{
					LoadDataDocumentURL = Method.CombineString(RelativePath, "/Data/", LoadDataDocumentName,".txt");
				}

				WWW www = new WWW( Method.ParseToRightDownloadPath(LoadDataDocumentURL) );

				yield return www;

				if (string.IsNullOrEmpty(www.error))
				{
					ClassJsonParser.ParseText(www, ref LoadData);	//Parse Json

					if (LoadData == null)
					{
						errorInfo = Method.CombineString("Parse Json Error : ", www.text);
					}
				}
				else
				{
					errorInfo = www.error;
				}
			}
			else
			{
				ClassJsonParser.ParseText(LoadDataDocument.text, ref LoadData);	//Parse Json

				if (LoadData == null)
				{
					errorInfo = Method.CombineString("Parse Json Error : ", LoadDataDocument.text);
				}
			}

			if (LoadData != null)
			{
				if (!IgnorePlaySetting)
					ShowStartInfo = LoadData.PlaySetting.ShowStartInfo;
				
				LoadData.SortData(RelativePath);
				StartPnael.SetData(LoadData.StartInfo);
				EndPnael.SetData(LoadData.EndInfo);

				BatchDownloadTool.ins.AddDownloadTask(this, LoadData.AudioPath, LoadData.AudioPath, DownloadType.WWWObject);
				BatchDownloadTool.ins.AddDownloadTask(this, LoadData.WordDataPath, LoadData.WordDataPath, DownloadType.Txt);
				BatchDownloadTool.ins.AddDownloadTask(this, LoadData.LyricsDataPath, LoadData.LyricsDataPath, DownloadType.Txt);
				BatchDownloadTool.ins.AddDownloadTask(this, LoadData.CommandDataPath, LoadData.CommandDataPath, DownloadType.Txt);
				BatchDownloadTool.ins.StartDownload();
			}
		}

		if (!string.IsNullOrEmpty(errorInfo))
		{
			ShowErrorMsg(errorInfo);
		}
	}

	public void ShowErrorMsg(string errorInfo)
	{
		MsgPnael.SetActive(true);
		MsgPnael.SetData(errorInfo);
	}

	public void OnDownloadComplete(DownloadFormat data)
	{
		if (data.Key == LoadData.AudioPath)
		{
			if (data.GotResourceSuccess)
				DataManager.ins.SongAudio = data.Resource.GetAudioClip(false);
		}
		else if (data.Key == LoadData.WordDataPath)
		{
			if (data.GotResourceSuccess)
				DataManager.ins.WordString = data.Resource.text;
		}
		else if (data.Key == LoadData.LyricsDataPath)
		{
			if (data.GotResourceSuccess)
				DataManager.ins.LyricsString = data.Resource.text;
		}
		else if (data.Key == LoadData.CommandDataPath)
		{
			if (data.GotResourceSuccess)
				DataManager.ins.CommandString = data.Resource.text;
		}
	}

	public void OnAllDownloadComplete()
	{
		StartCoroutine(_OnAllDownloadComplete());
	}

	IEnumerator _OnAllDownloadComplete()
	{
		//initialization
		{
			DataManager.ins.Init();
			AudioManager.ins.Init(LoadData.PlaySetting);
			RepeatReset();
		}

		//Fade
		{
			SimpleFadeTool.FadeOut(FadeDuration);
			yield return new WaitForSeconds(FadeDuration+0.1f);
			SimpleFadeTool.FadeIn(FadeDuration);
		}

		//StartBoard
		{
			DebugText.text = "";
			LoadingPnael.SetActive(false);
			EndPnael.SetActive(false);

			if (ShowStartInfo && AudioManager.ins.StartTime == 0)
			{
				//Show Board
				{
					SimpleFadeTool.FillColor();
					StartPnael.SetActive(true);
					SimpleFadeTool.FadeIn(FadeDuration);
				}

				yield return new WaitForSeconds(ShowTitleDuration);

				//Hide Board
				{
					SimpleFadeTool.FadeOut(FadeDuration);
					yield return new WaitForSeconds(FadeDuration+0.1f);
					StartPnael.SetActive(false);
				}
			}
			else
			{
				StartPnael.SetActive(false);
				yield return null;
			}
		}

		AudioManager.ins.PlayAudioAndLyrics(DataManager.ins.SongAudio);
	}
}
