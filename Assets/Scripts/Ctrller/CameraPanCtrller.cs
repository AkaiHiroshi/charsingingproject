﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CameraPanCtrller : CameraPanBase
{
	public List<GameObject> ManualObjs = new List<GameObject>();
	List<IUpdatePanValue> Interfaces = new List<IUpdatePanValue>();

	public void RegisterInterface(IUpdatePanValue inf)
	{
		if (inf == null) return;
		if (!Interfaces.Contains(inf))
			Interfaces.Add(inf);
	}
	public void UnregisterInterface(IUpdatePanValue inf)
	{
		if (inf == null) return;
		if (Interfaces.Contains(inf))
			Interfaces.Remove(inf);
	}

	protected override void Awake()
	{
		IUpdatePanValue inf = null;
		for (int i = 0, max = ManualObjs.Count; i < max; i++)
		{
			inf = ManualObjs[i].GetComponent<IUpdatePanValue>();
			RegisterInterface(inf);
		}

		base.Awake();
	}

	protected override void Update()
	{
		base.Update();

		for (int i = 0, max = Interfaces.Count; i < max; i++)
		{
			if (Interfaces[i] != null)
				Interfaces[i].UpdatePanValue(TransTarget.localEulerAngles.y, TransTarget.localEulerAngles.x, Degrees.x, Degrees.y);
		}
	}
}

public interface IUpdatePanValue
{
	void UpdatePanValue(float angleX, float angleY, float maxX, float maxY);
}