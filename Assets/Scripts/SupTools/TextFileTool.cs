using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;

#if !UNITY_WEB
using System.IO;
#endif

public enum TextFormat
{
	txt		=	0,
	xls		=	1,
	xml		=	2,
	dat		=	3,
}

public class TextFileTool
{
	static TextFileTool gInstance;
	public static TextFileTool ins
	{
		get {
			if (gInstance == null)
				gInstance = new TextFileTool();
			return gInstance;
		}
	}
	
	Encoding StringEncoding = Encoding.UTF8;

#if !UNITY_WEB

	/// <summary>
	/// 取得檔案的Text文字
	/// </summary>
	/// <returns>檔案的文字內容</returns>
	/// <param name="filePath">檔案路徑</param>
	public string GetTextFileInfo(string filePath)
	{
		if (!File.Exists(filePath))
		{
	#if UNITY_EDITOR
			Debug.LogWarning("路徑中不存在檔案 : " + filePath);
	#endif
			return "";
		}

		try
		{
			//		Debug.LogWarning (filePath);
			return File.ReadAllText(filePath, StringEncoding);
		}
		catch (System.IO.IOException IOEx)
		{
			Debug.LogError("<<讀取錯誤>>" + IOEx);
			return "";
		}
	}
	
	public void SaveTextFile(string[,] documentInfoArray, string storePath, string storeName, TextFormat format)
	{
		SaveTextFile(ParseArrayToDocument(documentInfoArray), storePath, storeName, format);
	}
	public void SaveTextFile(string[,] documentInfoArray, string fullPath)
	{
		SaveTextFile(ParseArrayToDocument(documentInfoArray), fullPath);
	}
	public void SaveTextFile(string documentInfoText, string fullPath)
	{
		string storeFileName	= GetLastCharString(fullPath, '/');
		string storePath		= fullPath.Replace(storeFileName, "");
		SaveTextFile(documentInfoText, storePath, storeFileName);
	}
	/// <summary>
	/// 儲存成文字文件
	/// </summary>
	/// <param name="documentInfoText">文件內容</param>
	/// <param name="storePath">儲存的路徑</param>
	/// <param name="storeName">儲存的檔案名稱</param>
	/// <param name="format">儲存的格式</param>
	public void SaveTextFile(string documentInfoText, string savePath, string saveName, TextFormat format)
	{
		SaveTextFile(documentInfoText, savePath, saveName+"."+format);
	}

	/// <summary>
	/// 儲存成文字文件
	/// </summary>
	/// <param name="documentInfoText">文件內容</param>
	/// <param name="storePath">儲存的路徑</param>
	/// <param name="storeFileName">檔案儲存的名稱(包含副檔名)</param>
	public void SaveTextFile(string documentInfo, string savePath, string saveFileName)
	{
		savePath = savePath.Replace("file://", "");

		string filePath = savePath + saveFileName;

		//創造一個目錄去儲存產生的資源包
		if (!Directory.Exists(savePath))
			Directory.CreateDirectory(savePath);

		if (File.Exists(filePath))
			File.Delete(filePath);

		try
		{
			TextWriter textWriter = new StreamWriter(filePath, false, StringEncoding);
			textWriter.Write(documentInfo);
			textWriter.Close();

	#if UNITY_EDITOR
			Debug.Log ("<color=yellow>"+ "已儲存檔案名「" + saveFileName + "」於路徑：" + filePath + "</color>");

			if (!Application.isPlaying)
				UnityEditor.AssetDatabase.Refresh();
	#endif
		}
		catch (System.IO.IOException IOEx)
		{
			Debug.LogError("<<寫入錯誤>>" + IOEx);
		}
	}
#endif

	/// <summary>
	/// 把表格文件轉換成二維陣列
	/// </summary>
	/// <returns>二維字串陣列</returns>
	/// <param name="documentText">表格文件的內容字串</param>
	public string[,] ParseDocumentToArray(string documentText, bool trimSpace = false, bool trimNewLine = true)
	{
		string[] documentLines = SplitIntoDocumentLines(documentText);
		
		int maxGridCount = 0;
		//判斷最後一排後面有沒有東西
		int lineCount = (string.IsNullOrEmpty(documentLines[documentLines.Length-1])) ? documentLines.Length-1 : documentLines.Length;

		string[] LineGrid = null;
		
		for (int lineIndex = 0; lineIndex < lineCount; lineIndex++)
		{
			LineGrid = SplitIntoLineGrids(documentLines[lineIndex]);
			//抓最大的格子數
			if (maxGridCount < LineGrid.Length)
				maxGridCount = LineGrid.Length;
		}
		
		string[,] result = new string[lineCount, maxGridCount];
		for (int i = 0; i < lineCount; i++)
		{
			LineGrid = SplitIntoLineGrids(documentLines[i]);
			for (int j = 0; j < maxGridCount; j++)
			{
				if (j <= LineGrid.Length-1)
				{
					result[i, j] = (trimSpace) ? LineGrid[j].Trim() : LineGrid[j];
					if (trimNewLine) result[i, j] = result[i, j];
				}
			}
		}
		return result;
	}
	//把二維陣列轉換為可以存的單一string
	public string ParseArrayToDocument(string[,] arrayText)
	{
		string result = "";
		
		for (int i = 0, iLineCount = arrayText.GetLength(0); i < iLineCount; i++)		//每一列
		{
			for (int j = 0, jGridCount = arrayText.GetLength(1); j < jGridCount; j++)	//每一格
			{
				if (j != jGridCount-1)	result += arrayText[i, j] + "\t";
				else 					result += arrayText[i, j] + "\n";
			}
		}
		return result;
	}

	//切文件，輸出單行
	public string[] SplitIntoDocumentLines(string documentText)
	{
		return documentText.Split('\n');
	}
	//切單行，輸出格子內容
	public string[] SplitIntoLineGrids(string lineText)
	{
		return lineText.Split('\t');
	}
	
	GUIStyle guiStyle = new GUIStyle();
	//計算Size
	public float GetTextWidth(int fontSize, string textInfo)
	{
		return GetTextWidth(null, fontSize, textInfo);
	}
	public float GetTextWidth(Font font, int fontSize, string textInfo)
	{
		guiStyle.font		= font;
		guiStyle.fontSize	= fontSize;
		
		Vector2 vSize = guiStyle.CalcSize(new GUIContent(textInfo));
		return vSize.x;
	}


	string GetLastCharString(string checkString, char checkChar)
	{
		string[] split = checkString.Split(checkChar);

		if (split.Length != 0)	return split[split.Length-1];
		else					return "";
	}
//--------------------------------------------------------------------------------------------------

}
