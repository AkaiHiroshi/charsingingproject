﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text;
using System.IO;

public interface IClearStruct
{
	void Clear();
}

public class Method
{
	static StringBuilder StaticSB;
	public static string CombineString(params object[] values)
	{
		string result = "";

		if (StaticSB == null)
			StaticSB = new StringBuilder();

		for (int i = 0; i < values.Length; i++)
		{
			if (values[i] != null)
				StaticSB.Append(values[i].ToString());
		}

		result = StaticSB.ToString();
		StaticSB.Length = 0;	//清空

		return result;
	}

	public static string ParseToRightDownloadPath(string localPathOrURL, bool addStringToAvoidCache = false)
	{
		string result = "";

		//要載Local端的資源
		if (!localPathOrURL.Contains("http") && !localPathOrURL.Contains("file://"))
		{
			if (File.Exists(localPathOrURL))
			{
				result = "file://" + localPathOrURL;
			}
			else
			{
				Debug.LogError("路徑資源不存在：" + localPathOrURL);
				result = "file://" + localPathOrURL;
			}
		}
		//WebPath
		else
		{
			result = localPathOrURL;

			if (addStringToAvoidCache)
			{
				if (!result.Contains("?p="))
					result = CombineString(localPathOrURL, "?p=", Random.Range(1, 1000000));
			}
		}

		return result;
	}

	public static T GetInstance<T>(bool autoGenerateNew = true) where T : UnityEngine.Component
	{
		T gInstance = GameObject.FindObjectOfType(typeof(T)) as T;

		if (gInstance == null)
		{
			string AddToSceneObjectName = typeof(T).GetType().Name;
			GameObject target = GameObject.Find(AddToSceneObjectName);

			//只允許在Runtime的時候生成新的
			if (autoGenerateNew && Application.isPlaying)
			{
				if (target == null) target = new GameObject(AddToSceneObjectName);
				gInstance = target.AddComponent<T>();
			}
		}
		return gInstance;
	}

	public static void AddArrayToList<T>(T[] addFrom, ref List<T> addTo)
	{
		if (addFrom == null || addTo == null)  return;

		for (int i = 0, max = addFrom.Length; i < max; i++)
		{
			if (addFrom[i] != null)
				addTo.Add( addFrom[i] );
		}
	}


	public static void ClearClassList<T>(ref List<T> list) where T: class
	{
		if (list != null)
		{
			IClearStruct iClass;
			for (int i = 0, count = list.Count; i < count; i++)
			{
				if (list[0] != null)
				{
					iClass = list[0] as IClearStruct;
					if (iClass != null) iClass.Clear();
				}

				list[0] = null;
				list.RemoveAt(0);
			}
			list.Clear();
		}
	}

	public static string ReverseString(string stringValue)
	{
		char[] charArray = stringValue.ToCharArray();
		System.Array.Reverse( charArray);
		return new string(charArray);
	}

	public static bool ParseValueToFloat(object objValue, out float outValue)
	{
		string strValue = objValue.ToString();

		float result = 0;
		bool success = false;

		if (!string.IsNullOrEmpty(strValue))
		{
			success = float.TryParse(strValue, out result);
		}
		else
		{
			success = true;
		}
		outValue = result;
		return success;
	}

	public static int ParseValueToInt(params object[] objValue)
	{
		int result = 0;

		if (objValue != null)
		{
			string strValue = "";

			if (objValue.Length == 1)	strValue = objValue[0].ToString();
			else						strValue = CombineString(objValue);

			bool success = false;

			if (!string.IsNullOrEmpty(strValue))
			{
				success = int.TryParse(strValue, out result);
			}
			else
			{
				success = true;
			}

			if (!success)
			{
//				Debug.LogError("Parse Int Failure : " + strValue);
			}
		}
		return result;
	}

	/// <summary>
	/// 取得兩個指定字元中間的字串
	/// </summary>
	/// <returns>
	/// 中間字串
	/// </returns>
	/// <param name='checkString'>
	/// 目標字串
	/// </param>
	/// <param name='checkChar1'>
	/// 目標字元
	/// </param>
	/// <param name='checkChar2'>
	/// 目標字元
	/// </param>
	public static string GetBetweenString(string checkString, char checkChar1, char checkChar2)
	{
		int fromI	= checkString.IndexOf(checkChar1)+1;
		int toI		= checkString.IndexOf(checkChar2);

		if (fromI < 0)	fromI	= 0;
		if (toI < 0)	toI		= checkString.Length-1;

		return checkString.Substring(fromI, toI-fromI);
	}

	/// <summary>
	/// 取得數字的統一命名規則 (比如說：目前值是7，最大值是105，結果就會補兩個0，變成007)
	/// </summary>
	/// <returns>
	/// 取得經過命名規則整理過的字串
	/// </returns>
	/// <param name='currentIndex'>
	/// 目前值
	/// </param>
	/// <param name='maxIndex'>
	/// 最大值 (用來看要補幾位數用)
	/// </param>
	public static string GetNumberIndexNameRule(int currentIndex, int maxIndex)
	{
		if (maxIndex < currentIndex) return currentIndex.ToString();

		int maxDigit		= maxIndex.ToString().Length;
		int currentDigit	= currentIndex.ToString().Length;

		string result = "";
		for (int i = 0, gap = maxDigit - currentDigit; i < gap; i++) result += "0";

		return result + currentIndex.ToString();
	}

	/// <summary>
	/// 取得float到小數點後幾位
	/// </summary>
	/// <returns>
	/// float到小數點後幾位的字串
	/// </returns>
	/// <param name='Value'>
	/// 原始數值
	/// </param>
	/// <param name='PointCount'>
	/// 小數點位數
	/// </param>
	public static string GetFloatString(float Value, int pointCount)
	{
		string strValue = Value.ToString();

		if (float.IsInfinity(Value) || float.IsNaN(Value))
		{
			return strValue;
		}

		if (strValue.Contains("."))
		{
			if (pointCount <= 0)
			{
				return strValue.Split('.')[0];
			}

			int cPointCount = strValue.Length - strValue.IndexOf(".") - 1;
			if (cPointCount > pointCount)
			{
				strValue = strValue.Substring(0, strValue.IndexOf(".") + pointCount + 1);
			}
			else if (cPointCount < pointCount)  	//補0
			{
				for (int i = 0, max = pointCount - (strValue.Length - strValue.IndexOf(".") - 1); i < max; i++)
				{
					strValue += "0";
				}
			}
		}
		else
		{
			if (pointCount > 0)
			{
				strValue += ".";
				for (int i = 0; i < pointCount; i++)
				{
					strValue += "0";
				}
			}
		}

		return strValue;
	}

	public static string GetNumberIndexNameRule(float currentIndex, float maxIndex, int pointCount)
	{
		if (maxIndex < currentIndex) return currentIndex.ToString();

		int maxDigit		= ((int)maxIndex).ToString().Length;
		int currentDigit	= ((int)currentIndex).ToString().Length;

		string frontZero = "";
		for (int i = 0, gap = maxDigit - currentDigit; i < gap; i++) 
		{
			frontZero += "0";
		}

		return frontZero + GetFloatString(currentIndex, pointCount);
	}

	/// <summary>
	/// 取得Component (有舊的就用舊的，沒有就裝上新的)
	/// </summary>
	/// <returns>
	/// 取得完畢的Component
	/// </returns>
	/// <param name='targetObject'>
	/// 要取得的物件目標
	/// </param>
	/// <typeparam name='T'>
	/// 要取得的Component
	/// </typeparam>
	public static T GetComponent<T>(GameObject targetObject) where T : UnityEngine.Component
	{
		if (targetObject == null) return null;
		T script = targetObject.GetComponent<T>();
		if (script) return script;
		script = targetObject.AddComponent<T>();

		return script;
	}

	//顯示每一個值的參數
	/// <summary>
	/// 把東西的值印出來 (單數格為Key，雙數格為Value)
	/// </summary>
	/// <returns>
	/// Log的字串
	/// </returns>
	/// <param name='logValues'>
	/// 要印出來的Key跟Value參數
	/// </param>
	public static string GetLogValueFormat(params object[] logValues)
	{
		string logString = "";

		int valueCount = logValues.Length;
		if (valueCount%2 != 0) valueCount--;	//不是成對就讓它成對

		for (int i = 0; i < valueCount; i++)
		{
			if (i%2 == 0)
			{
				logString = CombineString(logString, "[", ((logValues[i] == null) ? "NULL_KEY" : logValues[i].ToString()), " = ");
			}
			else
			{
				logString = CombineString(logString, ((logValues[i] == null) ? "NULL_VALUE" : logValues[i].ToString()), "]");
			}
		}
		return logString;
	}


	public static T IntStringToEnum<T>(string enumIntString) where T : struct
	{
		return IntToEnum<T>( ParseValueToInt(enumIntString) );
	}

	public static T IntToEnum<T>(int enumInt) where T : struct
	{
		if(!typeof(T).IsEnum)
		{
			throw new System.ArgumentException("丟入Type必須為列舉Type");
		}
		else
		{
			string enumString = System.Enum.GetName(typeof(T), enumInt);

			if (string.IsNullOrEmpty(enumString))
			{
				Debug.LogError( CombineString("列舉", typeof(T).Name ,"裡沒有此參數值：", enumInt));
				return default(T);
			}

			try
			{
				T result = (T)System.Enum.Parse(typeof(T), enumString);
				return result;
			}
			catch (System.Exception ex)
			{
				throw ex;
			}
		}
	}

	public static float GetAnimationClipLength(GameObject target, string clipName)
	{
		return GetAnimationClipLength(target.GetComponent<Animation>(), clipName);
	}
	public static float GetAnimationClipLength(Animation ani, string clipName)
	{
		if (ani != null)
		{
			AnimationState aState = ani[clipName];
			AnimationClip clip = ani.GetClip(clipName);

			if (clip != null)
			{
				if (aState == null) return clip.length;
				return clip.length/aState.speed;
			}
		}
		return -1;
	}

	public static void DelayActionNextFrame(System.Action callBack)
	{
		MethodCoroutineHandler.ins.DelayAction(Time.deltaTime, callBack);
	}
	public static void DelayAction(float duration, System.Action callBack)
	{
		MethodCoroutineHandler.ins.DelayAction(duration, callBack);
	}

	public static void DelayActionRealTimeNextFrame(System.Action callBack)
	{
		MethodCoroutineHandler.ins.DelayActionRealTime(Time.unscaledDeltaTime, callBack);
	}
	public static void DelayActionRealTime(float duration, System.Action callBack)
	{
		MethodCoroutineHandler.ins.DelayActionRealTime(duration, callBack);
	}
}
