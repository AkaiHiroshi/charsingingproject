using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using live2d;

[ExecuteInEditMode]
public abstract class Live2DModelBase: MonoBehaviour 
{
    public TextAsset MocFile ;
    public Texture2D[] TextureFiles;
    public TextAsset MotionFile;

	protected Live2DModelUnity Live2DModel;
	protected Live2DMotion Motion;
	protected MotionQueueManager MotionMgr;
	protected Matrix4x4 Live2DCanvasPos;

	protected void InitModel()
	{
		if (MocFile != null)
		{
			Live2DModel = Live2DModelUnity.loadModel(MocFile.bytes);

			for (int i = 0; i < TextureFiles.Length; i++)
			{
				if (TextureFiles[i] != null)
					Live2DModel.setTexture(i, TextureFiles[i]);
			}

			float modelWidth = Live2DModel.getCanvasWidth();
			Live2DCanvasPos = Matrix4x4.Ortho(0, modelWidth, modelWidth, 0, -50.0f, 50.0f);

			MotionMgr = new MotionQueueManager();

			if (MotionFile != null)
				Motion = Live2DMotion.loadMotion(MotionFile.bytes);
		}
		else
		{
			Debug.LogError("MocFile = null");
		}
	}

	protected abstract void UpdateMotionParam();

	protected virtual void Awake() 
	{
        Live2D.init();
		InitModel();
	}

	protected virtual void Update()
    {
        if (Live2DModel == null) return;
        Live2DModel.setMatrix(transform.localToWorldMatrix * Live2DCanvasPos);

        if (!Application.isPlaying)
        {
            Live2DModel.update();
            return;
        }

        if (MotionMgr.isFinished())
        {
            MotionMgr.startMotion(Motion);
        }

        MotionMgr.updateParam(Live2DModel);

        Live2DModel.update();
    }

	
	protected virtual void OnRenderObject()
	{
		if (Live2DModel == null)
		{
			InitModel();
		}

		if (Live2DModel != null)
		{
			Live2DModel.setMatrix(transform.localToWorldMatrix * Live2DCanvasPos);

			UpdateMotionParam();

			Live2DModel.update();
			Live2DModel.draw();
		}
	}
}