﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExpressionAnimationCtrller : SimpleAnimationCtrllerBase, IUpdateCharValueByCommand
{
	public bool EnableWink = true;
	public ExpressionCtrller MainCtrller;
	public Vector2 WinkIntervalDuration = new Vector2(4, 8);

	float TotalTimeCounter { get { return DataManager.ins.TotalTimeCounter; } }

	bool SkipWink = false;


	public void UpdateWithCommandData(List<ActionCommandDataInfo> data)
	{
		StopCoroutine("_UpdateWithCommandData");
		StartCoroutine(_UpdateWithCommandData(data));
	}

	IEnumerator _UpdateWithCommandData(List<ActionCommandDataInfo> data)
	{
		int index = 0;
		for (int i1 = 0, c1 = data.Count; i1 < c1; i1++)
		{
			if (TotalTimeCounter >= data[i1].Sec)
			{
				if (i1+1 < c1)
				{
					if (TotalTimeCounter < data[i1+1].Sec)
					{
						index = i1;
						break;
					}
				}
				else if (i1+1 == c1)		//Last
				{
					index = i1;
				}
			}
		}

		for (int i2 = index, c2 = data.Count; i2 < c2; i2++)
		{
			while (TotalTimeCounter < data[i2].Sec) yield return null;
			ExecuteCommand(data[i2]);
		}
	}

	void ExecuteCommand(ActionCommandDataInfo cmd)
	{
//		Debug.Log("ExecuteCommand : " + cmd);

		switch (cmd.Type)
		{
		case CommandType.PlayAnimation :

			SkipWink = cmd.SkipWink;
			PlayAnimation(cmd.KeyString);
			break;

		case CommandType.CallScript :
			MainCtrller.gameObject.SendMessage(cmd.KeyString, SendMessageOptions.RequireReceiver);
			break;
		}
	}

	void Wink()
	{
		if (Ani == null || !EnableWink) return;

		if (!SkipWink)
			PlayAnimation("Wink", true);
		
		Invoke("Wink", Random.Range(WinkIntervalDuration.x, WinkIntervalDuration.y));
	}

	void Awake()
	{
		if (MainCtrller == null)
			MainCtrller = gameObject.GetComponent<ExpressionCtrller>();
		if (Ani == null)
			Ani = gameObject.GetComponent<Animation>();
		
		AudioManager.ins.RegisterInterface(this);
	}

	void Start()
	{
		Invoke("Wink", Random.Range(WinkIntervalDuration.x, WinkIntervalDuration.y));
	}
}
