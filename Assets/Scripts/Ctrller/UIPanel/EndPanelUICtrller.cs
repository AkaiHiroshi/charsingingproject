﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class EndPanelUICtrller : PanelUICtrllerBase
{
	public Text Word;
	public Text Info;

	public void SetData(LoadDataInfo.EndPanelInfo data)
	{
		Word.text = data.Word;
		Info.text = data.Info;
	}
}
