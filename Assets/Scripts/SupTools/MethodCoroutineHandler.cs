﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//專門給別人代Call Coroutine方法的MonoBehaviour
public class MethodCoroutineHandler : MonoBehaviour
{
	static MethodCoroutineHandler gInstance = null;
	public static MethodCoroutineHandler ins {
		get {
			if (gInstance == null)
				gInstance = Method.GetInstance<MethodCoroutineHandler>();
			return gInstance;
		}
	}
	
	WaitForEndOfFrame WaitForEOF = new WaitForEndOfFrame();
	
	public void DelayAction(float duration, System.Action callBack)
	{
		StartCoroutine( _DelayAction(duration, callBack) );
	}
	
	public void DelayActionRealTime(float duration, System.Action callBack)
	{
		StartCoroutine( _DelayAction(duration, callBack, true) );
	}
	
	IEnumerator _DelayAction(float duration, System.Action callBack, bool realTime = false)
	{
		float time = 0;
		
		if (realTime)
		{
			time = Time.realtimeSinceStartup;
			
			while (Time.realtimeSinceStartup - time < duration)
			{
				yield return WaitForEOF;
			}
		}
		else
		{
			time = Time.time;
			
			while (Time.time - time < duration)
			{
				yield return WaitForEOF;
			}
		}
		
		callBack();
	}
}