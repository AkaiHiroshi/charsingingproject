﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WordDataDefine {}

public interface IUpdateMouth
{
	void ResetMouth();
	void UpdateMouthByWordData(WordDataInfo data, float nextWordIntervalDuration);
}
	
[System.Serializable]
public class WordDataInfo : IClearStruct
{
	public string Word;
	public bool IsValueEmpty = false;
	public List<WordValueDataInfo> ValueData = new List<WordValueDataInfo>();

	public WordDataInfo(string keyString, string weight, List<string> keys, List<string> values)
	{
		Word = keyString;

		if (keys.Count != values.Count)
		{
			Debug.LogError(keyString + " Value Count Error");
		}
		else
		{
			for (int i = 0, c1 = keys.Count; i < c1; i++)
			{
				if (string.IsNullOrEmpty(values[i])) continue;
				ValueData.Add( new WordValueDataInfo(keys[i], weight, values[i]) );
			}
			IsValueEmpty = (ValueData.Count == 0);
		}
	}

	public void Clear()
	{
		Method.ClearClassList(ref ValueData);
	}
}

[System.Serializable]
public class WordValueDataInfo : IClearStruct
{
	public string Key;
	public List<WordWeightValueDataInfo> WeightValueData = new List<WordWeightValueDataInfo>();

	public WordValueDataInfo(string key, string weight, string value)
	{
		Key = key;

		//WeightValue
		{
			bool averageWeight = (string.IsNullOrEmpty(weight));
			char splitChar = '_';
			WordWeightValueDataInfo valueData = null;

			string[] weightSplit = weight.Split(splitChar);
			string[] valueSplit = value.Split(splitChar);

			if (weightSplit.Length != valueSplit.Length)
			{
				averageWeight = true;
			}

			for (int i = 0, max = valueSplit.Length; i < max; i++)
			{
				valueData = new WordWeightValueDataInfo();

				if (!string.IsNullOrEmpty(valueSplit[i]))
				{
					Method.ParseValueToFloat(valueSplit[i], out valueData.Value);
				}

				if (averageWeight)
				{
					valueData.PlayRatio = 1f/(float)max;
				}
				else
				{
					if (!string.IsNullOrEmpty(weightSplit[i]))
					{
						Method.ParseValueToFloat(weightSplit[i], out valueData.PlayRatio);
					}
				}

				WeightValueData.Add(valueData);
			}
		}
	}

	public void Clear()
	{
		Method.ClearClassList(ref WeightValueData);
	}
}

[System.Serializable]
public class WordWeightValueDataInfo
{
	public float Value;
	public float PlayRatio = 1f;
}
