﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class DynamicLyricsCtrller : LyricsEffectCtrller
{
	public bool EnableShowEffect = true;
	public float ShowDuration = 1;
	public Text TargetText;

	public string text
	{
		get 
		{
			return (TargetText != null) ? TargetText.text : ""; 
		}
		set
		{
			if (TargetText != null)
			{
				if (EnableShowEffect && ShowDuration > 0 && !string.IsNullOrEmpty(value)) 
				{
					ShowLerpCutOffEffect(TargetText.text, value, ShowDuration);
				}
				else
				{
					TargetText.text = value;
				}
			}
		}
	}
}
