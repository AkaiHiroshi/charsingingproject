﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CommandDataDefine {}

public enum CommandType
{
	Void			=	-1,
	PlayAnimation	= 	 0,
	CallScript		=	 1,
}

public interface IUpdateCharValueByCommand
{
	void UpdateWithCommandData(List<ActionCommandDataInfo> data);
}

[System.Serializable]
public class ActionCommandDataInfo
{
	public CommandType Type;
	public bool SkipWink;
	public float Sec;
	public string KeyString;
	public string DocumentString;

	public ActionCommandDataInfo(string timeString, string commandType, string keyString, string skipWink)
	{
		Sec = DataFormat.ConvertTimeTextToSec(timeString);
		Type = (string.IsNullOrEmpty(commandType)) ? CommandType.Void : Method.IntStringToEnum<CommandType>(commandType);
		SkipWink = (Method.ParseValueToInt(skipWink) == 1);
		KeyString = keyString;

		UpdateDocumentString();
	}

	public void UpdateDocumentString()
	{
		DocumentString = Method.CombineString("[", DataFormat.GetTimeText(Sec), "]", "\t", ((int)Type).ToString(), "\t", KeyString, "\t", SkipWink ? "1" : "0");
	}

	public override string ToString()
	{
		return Method.GetLogValueFormat("Type", Type, "Sec", Sec, "KeyString", KeyString);
	}
}
