﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleAnimationCtrllerBase : MonoBehaviour
{
	public Animation Ani;

	public void PlayAnimation(string aniName, bool quitWhenPlaying = false, bool forcePlay = false)
	{
		StartCoroutine(_PlayAnimation(aniName, quitWhenPlaying, forcePlay));
	}

	protected IEnumerator _PlayAnimation(string aniName, bool quitWhenPlaying, bool forcePlay = false)
	{
		float aniLength = Method.GetAnimationClipLength(Ani, aniName);

		if (aniLength > 0)
		{
			if (forcePlay)
			{
				Ani.Play(aniName);
			}
			else
			{
				if (quitWhenPlaying)
				{
					if (!Ani.isPlaying)
					{
						Ani.Play(aniName);
					}
				}
				else
				{
					while (Ani.isPlaying)
					{
						yield return null;
					}

					Ani.Play(aniName);
				}
			}
		}
		else
		{
			Debug.LogError(aniName + " is not Animation Clip member");
		}
	}
}
