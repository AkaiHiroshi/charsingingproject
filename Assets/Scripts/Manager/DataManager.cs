﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class DataManager : MonoBehaviour, IClearStruct
{
	static DataManager gIns;
	public static DataManager ins {
		get  {
			if (gIns == null)
				gIns = Method.GetInstance<DataManager>();
			return gIns;
		}
	}

	public bool ParseDocumentWhenInit = true;
	public float TotalTimeCounter;

	//Song File
	public AudioClip SongAudio;

	//txt Files
	public TextAsset WordDocument;
	public TextAsset LyricsDocument;
	public TextAsset CommandDocument;

	[HideInInspector]
	public List<string> WordDataKeys = new List<string>();
	public List<WordDataInfo> WordData = new List<WordDataInfo>();
	public List<LyricsDataInfo> LyricsData = new List<LyricsDataInfo>();
	public List<ActionCommandDataInfo> CommandData = new List<ActionCommandDataInfo>();

	//String Data
	public string WordString	{ private get; set; }
	public string LyricsString	{ private get; set; }
	public string CommandString	{ private get; set; }


	public void Init()
	{
		if (ParseDocumentWhenInit)
		{
			if (string.IsNullOrEmpty(WordString))		ParseWordDocument();
			else 										ParseWordDocument(WordString);
			if (string.IsNullOrEmpty(LyricsString))		ParseLyricsDocument();
			else 										ParseLyricsDocument(LyricsString);
			if (string.IsNullOrEmpty(CommandString))	ParseCommandDocument();
			else 										ParseCommandDocument(CommandString);
		}
		else
		{
			if (WordDataKeys.Count == 0 || WordData.Count == 0) ParseWordDocument();
			if (LyricsData.Count == 0)							ParseLyricsDocument();
			if (CommandData.Count == 0)							ParseCommandDocument();
		}
	}

	public void SortOrder()
	{
		LyricsData = LyricsData.OrderBy(obj => obj.Sec).ToList();
		CommandData = CommandData.OrderBy(obj => obj.Sec).ToList();
	}

	[ContextMenu("Clear All Data")]
	public void Clear()
	{
		Method.ClearClassList(ref LyricsData);
		Method.ClearClassList(ref WordDataKeys);
		Method.ClearClassList(ref WordData);
		Method.ClearClassList(ref CommandData);
	}

	[ContextMenu("Parse Word Document")]
	bool ParseWordDocument()
	{
		if (WordDocument != null) return ParseWordDocument(WordDocument.text);

		Debug.LogError("WordDocument = null");
		return false;
	}
	public bool ParseWordDocument(string textInfo)
	{
		if (!string.IsNullOrEmpty(textInfo))
		{
			Method.ClearClassList(ref WordData);
			Method.ClearClassList(ref WordDataKeys);

			WordDataInfo data = null;
			string[,] textData = TextFileTool.ins.ParseDocumentToArray(textInfo);

			List<string> keys = new List<string>();
			List<string> values = new List<string>();

			for (int i1 = 0, max = textData.GetLength(1); i1 < max; i1++)
			{
				if (i1 > 1) keys.Add(textData[0, i1]);
			}

			for (int i2 = 1, iLineCount = textData.GetLength(0), jLineCount = textData.GetLength(1); i2 < iLineCount; i2++)
			{
				if (jLineCount < 3)
				{
					Debug.LogError("Document Format Error");
				}
				else
				{
					values.Clear();

					for (int i3 = 0; i3 < jLineCount; i3++)
					{
						if (i3 > 1) 
						{
							values.Add(textData[i2, i3]);
						}
					}

					string[] split = textData[i2, 0].Split(',');

					for (int j = 0, max = split.Length; j < max; j++)
					{
						if (!string.IsNullOrEmpty(split[j]))
						{
							data = new WordDataInfo(split[j], textData[i2, 1], keys, values);
							WordData.Add(data);
							WordDataKeys.Add(split[j]);
						}
					}
				}
			}
			return true;
		}

		Debug.LogError("ParseWordDocument : textInfo = null");
		return false;
	}

	[ContextMenu("Parse Lyrics Document")]
	bool _ParseLyricsDocument()
	{
		bool parseSuccess = false;

		if (WordData.Count == 0 || WordDataKeys.Count == 0)		parseSuccess = ParseWordDocument();
		else													parseSuccess = true;

		if (parseSuccess && LyricsDocument != null) return ParseLyricsDocument(LyricsDocument.text);

		Debug.LogError("ParseLyricsDocument = null / parseSuccess ? " + parseSuccess);
		return false;
	}

	bool ParseLyricsDocument()
	{
		if (LyricsDocument != null) return ParseLyricsDocument(LyricsDocument.text);

		Debug.LogError("ParseLyricsDocument = null");
		return false;
	}
	public bool ParseLyricsDocument(string textInfo)
	{
		if (!string.IsNullOrEmpty(textInfo))
		{
			Method.ClearClassList(ref LyricsData);

			LyricsDataInfo data = null;
			string[,] textData = TextFileTool.ins.ParseDocumentToArray(textInfo);

			for (int i = 1, iLineCount = textData.GetLength(0); i < iLineCount; i++)
			{
				data = new LyricsDataInfo(textData[i, 0], textData[i, 1], textData[i, 2], textData[i, 3]);

				if (data.Sec < 0)	data = null;
				else				LyricsData.Add(data);

				if (data != null && WordDataKeys.Count != 0 && WordData.Count != 0)
				{
					data.UpdateBracketsReplaceLyrics(WordDataKeys);
					data.UpdateDocumentString();
				}
			}

			//Sort
			LyricsData = LyricsData.OrderBy(obj => obj.Sec).ToList();
			return true;
		}

		Debug.LogError("ParseLyricsDocument : textInfo = null");
		return false;
	}

	[ContextMenu("Update Lyrics Document String")]
	void UpdateLyricsDocumentString()
	{
		for (int i = 0, max = LyricsData.Count; i < max; i++)
		{
			LyricsData[i].UpdateDocumentString();
		}
	}

	[ContextMenu("Update Command Document String")]
	void UpdateCommandDocumentString()
	{
		for (int i = 0, max = CommandData.Count; i < max; i++)
		{
			CommandData[i].UpdateDocumentString();
		}
	}
	[ContextMenu("Parse Command Document")]
	bool ParseCommandDocument()
	{
		if (CommandDocument != null) return ParseCommandDocument(CommandDocument.text);

		Debug.LogError("CommandDocument = null");
		return false;
	}
	public bool ParseCommandDocument(string textInfo)
	{
		if (!string.IsNullOrEmpty(textInfo))
		{
			Method.ClearClassList(ref CommandData);

			ActionCommandDataInfo data = null;
			string[,] textData = TextFileTool.ins.ParseDocumentToArray(textInfo);

			for (int i = 1, iLineCount = textData.GetLength(0); i < iLineCount; i++)
			{
				data = new ActionCommandDataInfo(textData[i, 0], textData[i, 1], textData[i, 2], textData[i, 3]);

				if (data.Sec < 0)	data = null;
				else				CommandData.Add(data);
			}

			//Sort
			CommandData = CommandData.OrderBy(obj => obj.Sec).ToList();
			return true;
		}

		Debug.LogError("ParseExpressionDocument : textInfo = null");
		return false;
	}

}
