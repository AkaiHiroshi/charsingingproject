﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataFormat
{
	public static string TempSymbol = "※";
	public const float EmptyValue = -999;

	public static bool IsBracketsCharacter(string charString)
	{
		return (charString == "(" || charString == ")" || charString == "（" || charString == "）");
	}

	public static bool IsSpaceCharacter(string charString)
	{
		return (charString == " " || charString == "　");
	}

	public static string ContainsChar(string checkString, string judgeString)
	{
		return (checkString.EndsWith(judgeString)) ? judgeString : "";
	}

	public static string GetTimeText(float time)
	{
		int hour = 0;
		int min = 0;
		float sec = time;

		while (sec > 3600)
		{
			hour++;
			sec -= 3600;
		}
		while (sec > 60)
		{
			min++;
			sec -= 60;
		}

		return Method.CombineString(hour, ":", Method.GetNumberIndexNameRule(min, 99), ":", Method.GetNumberIndexNameRule(sec, 99, 2));
	}

	public static float ConvertTimeTextToSec(string timeString)
	{
		float result = -1;

		if (!string.IsNullOrEmpty(timeString))		//Format -> [hh:mm:ss.999] or [mm:ss.999]
		{
			timeString = timeString.Replace("[", "").Replace("]", "");
			string[] split = timeString.Split(':');

			float tempSec = 0;
			int length = split.Length;

			switch (length)
			{
			case 1 :			//ex : [65.75] => 65.75 second

				Method.ParseValueToFloat(split[0], out result);
				break;

			case 2 :			//ex : [01:09.32] => 69.32 second

				Method.ParseValueToFloat(split[0], out tempSec);
				result = tempSec * 60;

				Method.ParseValueToFloat(split[1], out tempSec);
				result += tempSec;
				break;

			case 3 :			//ex : [01:03:20.92] => 69.32 second

				Method.ParseValueToFloat(split[0], out tempSec);
				result = tempSec * 60 * 60;

				Method.ParseValueToFloat(split[1], out tempSec);
				result += tempSec * 60;

				Method.ParseValueToFloat(split[2], out tempSec);
				result += tempSec;
				break;

			default :
				Debug.LogError("Time Format Error : " + timeString);
				break;
			}
		}

		return result;
	}
}

#region interface
#endregion
