﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Text;

public class BatchDownloadToolMethod : MonoBehaviour
{
	protected static T GetInstance<T>(bool autoGenerateNew = true) where T : UnityEngine.Component
	{
		T gInstance = GameObject.FindObjectOfType(typeof(T)) as T;

		if (gInstance == null)
		{
			string AddToSceneObjectName = typeof(T).GetType().Name;
			GameObject target = GameObject.Find(AddToSceneObjectName);

			//只允許在Runtime的時候生成新的
			if (autoGenerateNew && Application.isPlaying)
			{
				if (target == null) target = new GameObject(AddToSceneObjectName);
				gInstance = target.AddComponent<T>();
			}
		}
		return gInstance;
	}

	/// <summary>
	/// 取得目標路徑上檔案的大小
	/// </summary>
	/// <returns>
	/// 檔案大小
	/// </returns>
	/// <param name='filePath'>
	/// 路徑
	/// </param>
	public static int GetFileSize(string filePath)
	{
		int size = -1;

		if (File.Exists(filePath))
		{
			byte[] fileBytes = File.ReadAllBytes(filePath);
			size = fileBytes.Length;
			fileBytes = null;
		}
		else
		{
			Debug.LogWarning("路徑中不存在檔案 " + filePath);
		}
		return size;
	}

	/// <summary>
	/// 把Bytes寫入到本機端
	/// </summary>
	/// <returns>
	/// 儲存結果 "" or null = 成功  有字樣 = 失敗
	/// </returns>
	/// <param name='storeFilePath'>
	/// 儲存路徑
	/// </param>
	/// <param name='rawBytes'>
	/// 原始的Bytes
	/// </param>
	protected string WriteIntoLocal(string localPath, byte[] rawBytes)
	{
		#if !UNITY_WEB
		string storeFileName	= GetLastCharString(localPath, '/');
		string storePath		= localPath.Replace("/" + storeFileName, "");
		//		Debug.LogWarning (Method.GetLogValueFormat("storeFileName", storeFileName, "storePath", storePath));

		//創造一個目錄去儲存產生的資源包
		if (!Directory.Exists(storePath))
			Directory.CreateDirectory(storePath);

		if (File.Exists(localPath))
			File.Delete(localPath);

		try
		{
			File.WriteAllBytes(localPath, rawBytes);

		#if UNITY_EDITOR
			Debug.Log( CombineString("已儲存檔案名「", storeFileName, "」於路徑：", storePath, System.Environment.NewLine, "rawBytes : ", rawBytes.Length) );
			UnityEditor.AssetDatabase.Refresh();
		#endif
			return "";
		}
		catch (IOException IOEx)
		{
			return IOEx.ToString();
		}
		#endif
	}

	protected string ParseToRightDownloadPath(string localPathOrURL, bool addStringToAvoidCache = false)
	{
		string result = "";

		//要載Local端的資源
		if (!localPathOrURL.Contains("http") && !localPathOrURL.Contains("file://"))
		{
			if (File.Exists(localPathOrURL))
			{
				result = "file://" + localPathOrURL;
			}
			else
			{
				Debug.LogError("路徑資源不存在：" + localPathOrURL);
			}
		}
		//WebPath
		else
		{
			result = localPathOrURL;

			if (addStringToAvoidCache)
			{
				if (!result.Contains("?p="))
					result = CombineString(localPathOrURL, "?p=", Random.Range(1, 1000000));
			}
		}

		return result;
	}

	//顯示每一個值的參數
	/// <summary>
	/// 把東西的值印出來 (單數格為Key，雙數格為Value)
	/// </summary>
	/// <returns>
	/// Log的字串
	/// </returns>
	/// <param name='logValues'>
	/// 要印出來的Key跟Value參數
	/// </param>
	protected string GetLogValueFormat(params object[] logValues)
	{
		string logString = "";

		int valueCount = logValues.Length;
		if (valueCount%2 != 0) valueCount--;	//不是成對就讓它成對

		for (int i = 0; i < valueCount; i++)
		{
			if (i%2 == 0)
			{
				logString = CombineString(logString, "[", ((logValues[i] == null) ? "NULL_KEY" : logValues[i].ToString()), " = ");
			}
			else
			{
				logString = CombineString(logString, ((logValues[i] == null) ? "NULL_VALUE" : logValues[i].ToString()), "]");
			}
		}
		return logString;
	}


	/// <summary>
	/// 取得最後一個指定字元之後的字串
	/// </summary>
	/// <returns>
	/// 最後一個指定字元之後的字串
	/// </returns>
	/// <param name='checkString'>
	/// 目標字串
	/// </param>
	/// <param name='checkChar'>
	/// 目標字元
	/// </param>
	protected string GetLastCharString(string checkString, char checkChar)
	{
		string[] split = checkString.Split(checkChar);

		if (split.Length != 0)	return split[split.Length-1];
		else					return "";
	}



	protected StringBuilder StaticSB;
	protected string CombineString(params object[] values)
	{
		string result = "";

		if (StaticSB == null)
			StaticSB = new StringBuilder();

		for (int i = 0; i < values.Length; i++)
		{
			if (values[i] != null)
				StaticSB.Append(values[i].ToString());
		}

		result = StaticSB.ToString();
		StaticSB.Length = 0;	//清空

		return result;
	}

}
