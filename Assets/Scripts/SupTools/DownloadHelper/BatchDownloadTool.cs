﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

// 批次下載處理器
public class BatchDownloadTool : BatchDownloadToolMethod
{
#region Singleton
	static BatchDownloadTool gInstance = null;
	public static BatchDownloadTool ins
	{
		get {
			if (gInstance == null)
				gInstance = GetInstance<BatchDownloadTool>();
			return gInstance;
		}
	}
#endregion
	
#region 宣告、定義

	public enum State
	{
		Void			=	-1,
		Idle			=	0,	//閒置中
		Downloading		=	1,	//下載中
		Interrupted		=	2,	//下載中斷
	}
	public State CurrentState = State.Void;

	protected virtual void ChangeState(State toState)
	{
		if (CurrentState == toState) return;
		CurrentState = toState;

		switch (CurrentState)
		{
		case State.Idle :

			//把Pending住的東西倒到準備下載的List去
			if (PendingDownloadList.Count > 0)
			{
				List<IDownloadComplete> keys = new List<IDownloadComplete>(PendingDownloadList.Keys);

				for (int i = 0, keyCount = keys.Count; i < keyCount; i++)
				{
					for (int j = 0, max = PendingDownloadList[keys[0]].Count; j < max; j++)
					{
						AddDownloadTask( PendingDownloadList[keys[0]][0] );
						PendingDownloadList[keys[0]].RemoveAt(0);
					}
					PendingDownloadList.Remove(keys[0]);
					keys.RemoveAt(0);
				}
				StartDownload();
			}
			break;

		case State.Downloading :
			StartCoroutine(ExeDownload());
			break;
		}
	}

	protected int TotalCount = 0;
	protected WaitForEndOfFrame WaitForEOF = new WaitForEndOfFrame();
	
	protected List<IDownloadProgress> ProgressListeners = new List<IDownloadProgress>();

	protected Dictionary<IDownloadComplete, List<DownloadFormat>> DownloadList		= new Dictionary<IDownloadComplete, List<DownloadFormat>>();
	protected Dictionary<IDownloadComplete, List<DownloadFormat>> PendingDownloadList	= new Dictionary<IDownloadComplete, List<DownloadFormat>>();

#endregion
	
#region 公用函式
	
	//不比較檔案大小
	public void AddDownloadTask(IDownloadComplete caller, string taskName, string downloadURL, DownloadType downloadType)
	{
		AddDownloadTask(caller, taskName, downloadURL, "", 0, downloadType, DownloadHandleMethod.CacheOnly);
	}

	//不比較檔案大小
	public void AddDownloadTask(IDownloadComplete caller, string taskName, string downloadURL, string storePath, DownloadType downloadType, DownloadHandleMethod downloadHandleMethod)
	{
		AddDownloadTask(caller, taskName, downloadURL, storePath, 0, downloadType, DownloadHandleMethod.CacheOnly);
	}
	
	public void AddDownloadTask(IDownloadComplete caller, string taskName, string downloadURL, int dataSize, DownloadType downloadType)
	{
		AddDownloadTask(caller, taskName, downloadURL, "", dataSize, downloadType, DownloadHandleMethod.CacheOnly);
	}

	// 1.呼叫者(方便於下載完成後回傳)  2.任務名稱  3.下載的連結或位置  4.檔案的大小  5.下載的類型  6.下載到的路徑
	public void AddDownloadTask(IDownloadComplete caller, string key, string downloadURL, string fileStorePath, int fileSize, DownloadType downloadType, DownloadHandleMethod handleMethod)
	{
		AddDownloadTask( new DownloadFormat(caller, key, downloadURL, fileStorePath, fileSize, downloadType, handleMethod, caller.OnDownloadComplete) );
	}
	
	public void AddDownloadTask(DownloadFormat format)
	{
		if (CurrentState == State.Idle)
		{
			if (!DownloadList.ContainsKey(format.CompleteCallBack))
				 DownloadList.Add(format.CompleteCallBack, new List<DownloadFormat>());

			if (DownloadList[format.CompleteCallBack].Contains(format))
			{
				Debug.LogWarning("已經存在於下載清單中");
				return;
			}
			else
			{
				format.DownloadProgress = 0;
				format.Status = DonwloadStatus.Idle;
				DownloadList[format.CompleteCallBack].Add(format);
				
//				Debug.Log("加入任務 " + format.Key + " 到下載清單中");
			}
		}
		else 	//下載中加入的話就加到PendingList中等待下載
		{
			if (!PendingDownloadList.ContainsKey(format.CompleteCallBack))
				PendingDownloadList.Add(format.CompleteCallBack, new List<DownloadFormat>());

			if (PendingDownloadList[format.CompleteCallBack].Contains(format))
			{
				Debug.LogWarning("已經存在於Pending的下載清單中");
				return;
			}
			else
			{
//				Debug.Log("加入任務 " + format.Key + " 到Pending的下載清單中");
				PendingDownloadList[format.CompleteCallBack].Add(format);
			}
		}
	}
	
	public void RegisterProgressCallBack(IDownloadProgress i)
	{
		if (!ProgressListeners.Contains(i))
			 ProgressListeners.Add(i);
	}
	public void UnregisterProgressCallBack(IDownloadProgress i)
	{
		if (ProgressListeners.Contains(i))
			ProgressListeners.Remove(i);
	}

	public IEnumerator SingleDownload(DownloadFormat format, bool doCallBack = true)
	{
		bool exeDownload = true;
		string url = ParseToRightDownloadPath(format.DownloadURL);

		if (format.HandleMethod == DownloadHandleMethod.CacheOnly)
		{
			//有Clinet的就先拿Client的
			if (!string.IsNullOrEmpty(format.FileStorePath))
			{
				if (File.Exists(format.FileStorePath))
				{
					url = ParseToRightDownloadPath(format.FileStorePath);
				}
			}
		}
		else
		{
			//要儲存卻沒給儲存路徑
			if (string.IsNullOrEmpty(format.FileStorePath))
			{
				format.SetErrorInfo(DownloadErrorType.FileStorePathNull);
				exeDownload = false;
			}
			else
			{
				//儲存路徑上已經有檔案
				if (File.Exists(format.FileStorePath))
				{
					//只要儲存的話就可以滾了
					if (format.HandleMethod == DownloadHandleMethod.StoreOnly)
					{
						exeDownload = false;
					}
					//抓路徑上的檔案
					else
					{
						url = ParseToRightDownloadPath(format.FileStorePath);
					}
				}
			}
		}

		if (exeDownload)
		{
//			Debug.LogError("exeDownload : " + format.FileName);
			Caching.CleanCache();

			WWW www = new WWW(url);

			while (!www.isDone)
			{
				format.DownloadProgress = www.progress;
				UpdateDownloadProgress();
				yield return WaitForEOF;
			}
			
			if (string.IsNullOrEmpty(www.error))
			{
				//不儲存
				if (format.HandleMethod == DownloadHandleMethod.CacheOnly)
				{
					format.DownloadProgress = 1;
					format.Resource = www;
				}
				//儲存
				else
				{
					//要儲存卻沒給儲存路徑
					if (string.IsNullOrEmpty(format.FileStorePath))
					{
						format.SetErrorInfo(DownloadErrorType.FileStorePathNull);
					}
					else
					{
						string errorMsg = "";

						//已經有檔案存在路徑上
						if (File.Exists(format.FileStorePath))
						{
							//如果兩者檔案大小不一致則需要複寫
							if (format.FileSize > 0 && GetFileSize(format.FileStorePath) != format.FileSize)
							{
								errorMsg = WriteIntoLocal(format.FileStorePath, www.bytes);
							}
						}
						else
						{
							errorMsg = WriteIntoLocal(format.FileStorePath, www.bytes);
						}

						if (string.IsNullOrEmpty(errorMsg))
						{
							format.DownloadProgress = 1;
							format.Status = DonwloadStatus.Success;

							if (format.HandleMethod == DownloadHandleMethod.CacheAndStore)
							{
								format.Resource = www;
							}
							else
							{
								www = null;
								format.Resource = null;
							}
						}
						else
						{
							format.SetErrorInfo(DownloadErrorType.WhiteInError, errorMsg);
						}
					}
				}
			}
			else
			{
				if (url.Contains("file://"))
				{
					format.SetErrorInfo(DownloadErrorType.DownloadFailure, www.error);
				}
				else
				{
					if (IsNetworkConnected())		format.SetErrorInfo(DownloadErrorType.DownloadFailure, www.error);
					else							format.SetErrorInfo(DownloadErrorType.NoNetwork);
				}
			}
		}

		if (doCallBack)
		{
			if (format.OnDownloadComplete != null)
				format.OnDownloadComplete(format);
		}
	}

	protected virtual bool IsNetworkConnected()
	{
		switch (Application.internetReachability)
		{
		case NetworkReachability.NotReachable : 
			return false;
		case NetworkReachability.ReachableViaLocalAreaNetwork :		// wifi or 網路線
			Debug.Log("wifi or normal connection");
			return true;
		case NetworkReachability.ReachableViaCarrierDataNetwork :	// 3G or 4G
			Debug.Log("mobile 3G or 4G connection");
			return true;
		}
		return false;
	}

	//執行下載動作
	public void StartDownload()
	{
		ChangeState(State.Downloading);
	}

#endregion
	
	protected virtual IEnumerator ExeDownload()
	{
		Caching.CleanCache();

		TotalCount = 0;

		foreach (IDownloadComplete caller in DownloadList.Keys)
		{
			TotalCount += DownloadList[caller].Count;
		}

		if (TotalCount == 0)
		{
			Debug.LogError("NoDownloadList -> TotalCount = " + DownloadList.Count);
			OnTotalDownloadComplete();
		}
		else
		{
			foreach (IDownloadComplete caller in DownloadList.Keys)
			{
				for (int i = 0, max = DownloadList[caller].Count; i < max; i++)
				{
					if (CurrentState != State.Downloading) break;

					if (DownloadList[caller][i].Status == DonwloadStatus.Success || 
					    DownloadList[caller][i].Status == DonwloadStatus.NeedNotToDownload) continue;

					DownloadFormat info = DownloadList[caller][i];

					info.DownloadProgress = 0;
					UpdateDownloadProgress();
					
//					Debug.Log(GetLogValueFormat("Key", info.Key, "FileName", info.FileName, "DownloadType", info.DownloadType, "HandleMethod", info.HandleMethod, "URL", info.DownloadURL, "FileStorePath", info.FileStorePath, "FileSize", info.FileSize));
					
					switch (info.DownloadType)
					{
					case DownloadType.DoNotDownload :
					case DownloadType.ValueOnly :
						info.Status = DonwloadStatus.NeedNotToDownload;
						continue;

					case DownloadType.Txt :
					case DownloadType.Assetbundle :
					case DownloadType.WWWObject :

						do
						{
							if (NeedDownload(info))
							{
								yield return StartCoroutine( SingleDownload(info, false) );
							}

							int fileSize = info.ResourceFileSize;
							
							//取得檔案大小相等or不用比較
							if (fileSize == info.FileSize || info.FileSize <= 0 || info.Status == DonwloadStatus.HasError)
							{
								break;
							}
							//檔案損壞或是被更動就重新載入一次
							else
							{
							#if UNITY_EDITOR
								Debug.LogWarning ("<<Size不相同>> " + GetLogValueFormat("TaskName", info.Key, "雲端檔案大小", info.FileSize, "目前Client端檔案大小", fileSize, "下載網址", info.DownloadURL));
							#endif
								
								if (!string.IsNullOrEmpty(info.FileStorePath))
								{
									if (File.Exists(info.FileStorePath))
										File.Delete(info.FileStorePath);
								}
							}

						} while (true);
						break;
					}
					
				#if UNITY_IPHONE
					if (!string.IsNullOrEmpty(info.FileStorePath))
					{
						iPhone.SetNoBackupFlag(info.FileStorePath);
					}
				#endif

					if (info.Status == DonwloadStatus.HasError)
					{
						OnDownloadError(info.ErrorInfo);
						ChangeState(State.Interrupted);
						return false;
					}
					else
					{
						info.SetComplete();
					}
				}
			}
			
//			Debug.LogError("OnTotalDownloadComplete!");
			OnTotalDownloadComplete();
		}
	}
	
	protected void UpdateDownloadProgress(float totalProgress = 0)
	{
		if (ProgressListeners.Count == 0) return;
		
		if (totalProgress == 0 && TotalCount != 0)
		{
			foreach (IDownloadComplete caller in DownloadList.Keys)
			{
				for (int i = 0; i < DownloadList[caller].Count; i++)
				{
					totalProgress += DownloadList[caller][i].DownloadProgress;
				}
			}

			totalProgress = totalProgress/(float)TotalCount;
		}

		foreach(IDownloadProgress idp in ProgressListeners)
		{
			if (idp != null)
				idp.OnTotalProgress(totalProgress);
		}
	}

	
	protected virtual void OnDownloadError(DownloadErrorInfo errorInfo)
	{
		Debug.LogError("DownError : " + errorInfo.ToString());
	}

	protected void OnTotalDownloadComplete()
	{
		UpdateDownloadProgress(1);
		
		//告知下載完畢
		foreach (IDownloadComplete caller in DownloadList.Keys)
		{
			caller.OnAllDownloadComplete();
			
			for (int i = 0, max = DownloadList[caller].Count; i < max; i++)
			{
				DownloadList[caller].RemoveAt(0);
			}
			DownloadList[caller].Clear();
		}
		DownloadList.Clear();

		ChangeState(State.Idle);
	}
	
	protected bool NeedDownload(DownloadFormat format)
	{
		bool result = true;
		
		if (format.HandleMethod != DownloadHandleMethod.CacheOnly)
		{
			//要儲存卻沒給儲存路徑
			if (string.IsNullOrEmpty(format.FileStorePath))
			{
				format.SetErrorInfo(DownloadErrorType.FileStorePathNull);
				result = false;
			}
			else
			{
				//儲存路徑上已經有檔案
				if (File.Exists(format.FileStorePath))
				{
					//只要儲存的話就可以滾了
					if (format.HandleMethod == DownloadHandleMethod.StoreOnly)
					{
						result = false;
					}
				}
			}
		}
		return result;
	}

	protected virtual void Awake()
	{
		if (gInstance == null)
			gInstance = this;

		ChangeState(State.Idle);
	}
}