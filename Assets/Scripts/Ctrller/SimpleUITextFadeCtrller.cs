﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SimpleUITextFadeCtrller : MonoBehaviour
{
	public Text Label;

	public void Fade(float duration, float toValue, System.Action callBack = null)
	{
		if (Label == null)
			Label = GetComponent<Text>();

		if (Label != null)
			Fade(duration, Label.color.a, toValue, callBack);
	}
	public void Fade(float duration, float fromValue, float toValue, System.Action callBack = null)
	{
		StopAllCoroutines();

		if (Label == null)
			Label = GetComponent<Text>();
				
		if (Label != null)
		{
			Color color = Label.color;
			color.a = fromValue;
			Label.color = color;

			StartCoroutine( LerpFloatTool.ins.GetValue(duration, fromValue, toValue, (float value) =>
			{
				color.a = value; 
				Label.color = color;

				if (value == toValue)
				{
					if (callBack != null)
						callBack();
				}
			} ) );
		}
	}
}
