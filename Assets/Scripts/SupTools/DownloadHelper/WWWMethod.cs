﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class WWWMethod
{
	public static T UnpackWWW<T>(WWW www, string assetbundleKey) where T : Object
	{
		if (www == null)						return null;
		if (!string.IsNullOrEmpty(www.error))	return null;
		if (www.assetBundle == null)			return null;
		
		T result = www.assetBundle.LoadAsset<T>(assetbundleKey);
		www.assetBundle.Unload(false);	//釋放資源
		return result;
	}
	
	public static Texture2D UnpackWWWImage(WWW www, string assetbundleKey)
	{
		if (www == null)						return null;
		if (!string.IsNullOrEmpty(www.error))	return null;
		if (www.assetBundle == null)			return www.texture;
		
		Texture2D result = www.assetBundle.LoadAsset<Texture2D>(assetbundleKey);
		www.assetBundle.Unload(false);	//釋放資源
		return result;
	}

	public static string FilterWWWText(WWW www)
	{
		string result = "";
		if (string.IsNullOrEmpty(www.error))
			result = System.Text.Encoding.UTF8.GetString(www.bytes, 3, www.bytes.Length - 3);  // Skip thr first 3 bytes (i.e. the UTF8 BOM)
		return result;
	}

	/// <summary>
	/// 比較本機端的檔案版本
	/// </summary>
	/// <returns>與Client版本是否相同</returns>
	/// <param name="version">Version.</param>
	/// <param name="filePath">File path.</param>
	/// <param name="deleteFileWhenVersionNotTheSame">要自動把不符合現在版本的物件刪除嗎?</param>
	public static bool CompareVersion(float version, string filePath, bool deleteFileWhenVersionNotTheSame = true)
	{
		bool result = false;
		bool deleteFile = false;
		
		//不論如何都重新載入
		if (version <= 0)
		{
			deleteFile = true;
		}
		else
		{
			if (PlayerPrefs.HasKey(filePath))
			{
				result = (PlayerPrefs.GetFloat(filePath) == version);
				deleteFile = !result;
			}
			PlayerPrefs.SetFloat(filePath, version);	//紀錄
		}

		if (deleteFile && deleteFileWhenVersionNotTheSame)
		{
			if (!string.IsNullOrEmpty(filePath))
			{
				if (File.Exists(filePath))
					File.Delete(filePath);
			}
		}
		return result;
	}
}

