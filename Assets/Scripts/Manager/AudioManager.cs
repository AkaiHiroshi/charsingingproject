﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AudioManager : SceneDataBase
{
	static AudioManager gIns;
	public static AudioManager ins {
		get  {
			if (gIns == null)
				gIns = Method.GetInstance<AudioManager>();
			return gIns;
		}
	}

	public bool AudioRepeat = false;
	public bool ShowLanguageLyrics = true;
	public bool ShowTimeText = false;
	public bool ShowLyricsEffect = true;
	public bool ShowHiragana = false;

	[Range(0.25f, 1.5f)]
	public float PlaySpeed = 1;
	public float StartTime = 0;
	public float EndTime = -1;
	public float FadeDuration = 0.5f;
	public float LyricsOffsetDuration = -0.2f;
	[Range(0, 1)]
	public float DynamicLyricsEndRatio = 0.8f;
	public AudioSource AudioPlayer;

	public Text TimeText;
	public Text LyricsText;
	public Text LangLyricsText;
	public Text DynamicLyricsText;
	public Transform WebParent;
	public LyricsEffectCtrller DynamicLyricsCtrller;

	[SerializeField]
	List<string> NeedToAddValueList = new List<string>();

	int Index = 0;
	List<IUpdateMouth> InfList = new List<IUpdateMouth>();
	List<IUpdateCharValueByCommand> CmdInfList = new List<IUpdateCharValueByCommand>();

#region Public Method

	public void Init(LoadDataInfo.PlaySettingInfo playSetting)
	{
		if (!SceneManager.ins.IgnorePlaySetting)
		{
			AudioRepeat = playSetting.AudioRepeat;
			ShowTimeText = playSetting.ShowTimeText;
			ShowHiragana = playSetting.ShowHiragana;
			ShowLyricsEffect = playSetting.ShowLyricsEffect;
			ShowLanguageLyrics = playSetting.ShowLanguageLyrics;
			StartTime = playSetting.StartTime;
			EndTime = playSetting.EndTime;
		}

		Time.timeScale = PlaySpeed;
		AudioPlayer.pitch = Time.timeScale;

		Color color = LangLyricsText.color;
		color.a = 0;
		LangLyricsText.color = color;

		DynamicLyricsText.text = "";
		LangLyricsText.text = "";
		LyricsText.text = "";

	#if UNITY_WEBGL || UNITY_WEB

		//UIMask not suppose
		ShowLyricsEffect = false;
		DynamicLyricsText.transform.parent = WebParent;
	#endif
	}

	public void RegisterInterface(IUpdateMouth inf)
	{
		if (inf == null) return;
		if (!InfList.Contains(inf))
			InfList.Add(inf);
	}
	public void RegisterInterface(IUpdateCharValueByCommand inf)
	{
		if (inf == null) return;
		if (!CmdInfList.Contains(inf))
			CmdInfList.Add(inf);
	}

	public void PlayAudioAndLyrics(AudioClip audio)
	{
		DataManager.ins.SortOrder();
		StopAllCoroutines();
		StartCoroutine( _PlayAudioAndLyrics(audio) );
	}
	IEnumerator _PlayAudioAndLyrics(AudioClip audio)
	{
		if (audio != null)
		{
			if (StartTime >= audio.length)	StartTime = 0;
			if (EndTime <= 0)				EndTime = audio.length;
			
			AudioPlayer.clip = audio;
			AudioPlayer.time = StartTime;
			AudioPlayer.Play();

			Index = 0;

			TotalTimeCounter = StartTime;
			StartCoroutine( UpdateTotalTime() );

			for (int i1 = 0, c1 = CmdInfList.Count; i1 < c1; i1++)
			{
				CmdInfList[i1].UpdateWithCommandData(ExpressionData);
			}

			for (int i2 = 0, c2 = LyricsData.Count; i2 < c2; i2++)
			{
				if (TotalTimeCounter >= LyricsData[i2].Sec)
				{
					if (i2+1 < c2)
					{
						if (TotalTimeCounter < LyricsData[i2+1].Sec)
						{
							Index = i2;
							break;
						}
					}
					else if (i2 == c2-1)		//Last
					{
						Index = i2;
					}
				}
			}

//			Debug.LogError("START INDEX = " + Index);
			for (int i3 = Index, c3 = LyricsData.Count; i3 < c3; i3++)
			{
				yield return StartCoroutine( PlayLyrics(LyricsData[i3]) );

				if (i3 < c3-1)
				{
					float waitTime = (EndTime < LyricsData[i3+1].Sec) ? EndTime : LyricsData[i3+1].Sec;

					while (TotalTimeCounter < waitTime)
					{
						yield return null;
					}

					if (TotalTimeCounter >= EndTime)
					{
						break;
					}
				}
			}

			while (TotalTimeCounter < EndTime)
			{
				yield return null;
			}
			AudioPlayer.Stop();

			if (AudioRepeat)
			{
				SceneManager.ins.RepeatReset();
				PlayAudioAndLyrics(audio);
			}
		}
		else
		{
			Debug.LogError("Audio Clip = NULL");
		}
	}
#endregion

	IEnumerator UpdateTotalTime()
	{
		while (TotalTimeCounter < EndTime)
		{
			TotalTimeCounter += Time.deltaTime;
			TimeText.text = (ShowTimeText) ? DataFormat.GetTimeText(TotalTimeCounter) + " / " + Method.GetFloatString(TotalTimeCounter, 2) : "";
			yield return null;
		}
	}

	IEnumerator PlayLyrics(LyricsDataInfo data)
	{
		int fromInex = 0;
		float sumDuration = 0;
		string toText = "";

		while (TotalTimeCounter < data.Sec)
		{
			yield return null;
		}

		DynamicLyricsText.text = "";
		LangLyricsText.text = (ShowLanguageLyrics) ? data.LangLyrics : "";
		LyricsText.text = (ShowHiragana) ? data.BracketsReplaceLyrics : data.BracketsRemoveLyrics;

		Method.GetComponent<SimpleUITextFadeCtrller>(LangLyricsText.gameObject).Fade(FadeDuration, 1);

		for (int i1 = 0, c1 = data.CharLyricsData.Count; i1 < c1; i1++)
		{
//			Debug.LogWarning(data.Sec + " / " + sumDuration + " / " + TotalTimeCounter);

			if (data.Sec + sumDuration + data.CharLyricsData[i1].PlayDuration >= TotalTimeCounter)
			{
				fromInex = i1;
				break;
			}
			else
			{
				sumDuration += data.CharLyricsData[i1].PlayDuration;

				if (ShowHiragana)
				{
					DynamicLyricsText.text += data.CharLyricsData[i1].Character;
				}
				else
				{
					if (data.CharLyricsData[i1].CorrOriWordIndex == 0)
					{
						DynamicLyricsText.text += data.CharLyricsData[i1].CorrOriWord;
					}
					else if (data.CharLyricsData[i1].CorrOriWordIndex > 0)
					{
					}
					else
					{
						DynamicLyricsText.text += data.CharLyricsData[i1].Character;
					}
				}

//				Debug.Log(Method.GetLogValueFormat("sumDuration", sumDuration, "PlayDuration", data.CharLyricsData[i1].PlayDuration, "Character", data.CharLyricsData[i1].Character, "CorrOriWord", data.CharLyricsData[i1].CorrOriWord, "DynamicLyricsText.text", DynamicLyricsText.text));
			}
		}

		for (int i2 = fromInex, c2= data.CharLyricsData.Count; i2 < c2; i2++)
		{
			if (TotalTimeCounter < EndTime)
			{
				SendMouthWordData(data.CharLyricsData[i2].CharacterWithoutSpace, data.CharLyricsData[i2].PlayDuration);

				sumDuration += data.CharLyricsData[i2].PlayDuration;

				if (ShowHiragana)
				{
					toText = DynamicLyricsText.text + data.CharLyricsData[i2].Character;
				}
				else
				{
					if (data.CharLyricsData[i2].CorrOriWordIndex == 0)
					{
//						Debug.LogWarning("["+data.CharLyricsData[i2].CorrOriWord+"]");
						toText = DynamicLyricsText.text + data.CharLyricsData[i2].CorrOriWord;

						toText += DataFormat.ContainsChar(data.CharLyricsData[i2].Character, " ");
						toText += DataFormat.ContainsChar(data.CharLyricsData[i2].Character, "　");
					}
					else if (data.CharLyricsData[i2].CorrOriWordIndex > 0)
					{
//						Debug.LogError(toText);
						if (string.IsNullOrEmpty(toText))
						{
							toText = DynamicLyricsText.text;
						}

						toText += DataFormat.ContainsChar(data.CharLyricsData[i2].Character, " ");
						toText += DataFormat.ContainsChar(data.CharLyricsData[i2].Character, "　");
					}
					else
					{
//						Debug.Log("["+data.CharLyricsData[i2].Character+"]");
						toText = DynamicLyricsText.text + data.CharLyricsData[i2].Character;
					}
				}

				if (DynamicLyricsCtrller != null)
				{
					if (ShowLyricsEffect)	//Show like animation
					{
						float showDuration = 0;
						
						if (ShowHiragana)
						{
							showDuration = data.CharLyricsData[i2].PlayDuration;
						}
						else
						{
							if (data.CharLyricsData[i2].CorrOriWordIndex == 0)
							{
								showDuration += data.CharLyricsData[i2].PlayDuration;

								int tempIndex = i2;
								while (tempIndex+1 < c2)			//CheckNexChar
								{
									if (data.CharLyricsData[tempIndex+1].CorrOriWordIndex > 0)
									{
										showDuration += data.CharLyricsData[tempIndex+1].PlayDuration;
										tempIndex++;
									}
									else
									{
										break;
									}
								}
							}
							else if (data.CharLyricsData[i2].CorrOriWordIndex > 0)
							{
							}
							else
							{
								showDuration = data.CharLyricsData[i2].PlayDuration;
							}
						}

						showDuration *= DynamicLyricsEndRatio;
						DynamicLyricsCtrller.ShowLerpCutOffEffect(DynamicLyricsText.text, toText, showDuration);
					}
					else
					{
						DynamicLyricsCtrller.Width = 4096;
					}
				}

//				Debug.Log(DynamicLyricsText.text + "=>" + toText);
				DynamicLyricsText.text = toText;

				while (TotalTimeCounter < data.Sec + sumDuration + LyricsOffsetDuration)
				{
					yield return null;

					if (TotalTimeCounter >= EndTime)
					{
						break;
					}
				}
			}
			else
			{
				break;
			}
		}

		for (int i = 0, max = InfList.Count; i < max; i++)
		{
			InfList[i].ResetMouth();
		}

		LyricsText.text = "";
		DynamicLyricsText.text = "";
		Method.GetComponent<SimpleUITextFadeCtrller>(LangLyricsText.gameObject).Fade(FadeDuration, 0);
	}

	void SendMouthWordData(string charString, float nextWordIntervalDuration)
	{
		if (WordDataKeys.Contains(charString))
		{
			WordDataInfo data = WordData[WordDataKeys.IndexOf(charString)];

			if (data != null)
			{
				if (!data.IsValueEmpty)
				{
					for (int i = 0, max = InfList.Count; i < max; i++)
					{
						InfList[i].UpdateMouthByWordData(data, nextWordIntervalDuration);
					}
				}
				else
				{
					if (!NeedToAddValueList.Contains(charString))
					{
						NeedToAddValueList.Add(charString);
						Debug.LogWarning("Need To Add Value String : "+ charString);
					}
				}
			}
		}
	}

	void Awake()
	{
		if (gIns == null)
			gIns = this;
	}
}
