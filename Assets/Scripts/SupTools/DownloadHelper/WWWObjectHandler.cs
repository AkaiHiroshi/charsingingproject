﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WWWObjectHandler : MonoBehaviour
{
	static WWWObjectHandler gInstance;
	public static WWWObjectHandler GetInstance()
	{
		if (gInstance == null)
		{
			gInstance = GameObject.FindObjectOfType(typeof(WWWObjectHandler)) as WWWObjectHandler;
			
			if (gInstance == null)
				gInstance = new GameObject("Script_WWWObjectHandler").AddComponent<WWWObjectHandler>();
		}
		return gInstance;
	}
	
	[System.Serializable]
	public class RequestFormat
	{
		public string		StoreName				= "";		//WWW儲存的名稱
		public string		RequestURL				= "";		//下載的位置
		public GameObject	CallBackObject			= null;		//下載完畢時回Call的物件
		public string		CallBackMethod			= "";		//下載完畢時回Call的方法名稱
		public int			Version					= 0;
		public string		FailedCallBackMethod	= "";		//下載失敗回傳的方法
	}
	
	[SerializeField]
	public List<RequestFormat> list_Request = new List<RequestFormat>();
	List<RequestFormat> list_WaitForCallBack = new List<RequestFormat>();		//等待回傳的列表
	WaitForEndOfFrame WaitForEOF = new WaitForEndOfFrame();
	
	/// <summary>
	/// 下載回來的WWW資料
	/// </summary>
	Dictionary<string, WWW> dic_DownloadData = new Dictionary<string, WWW>();
	
	public void method_DownloadObject(string requestURL, string storeName, GameObject callBackObject, string callBackMethod, string failedCallBackMethod = "")
	{
		method_DownloadObject(requestURL, storeName, callBackObject, callBackMethod, -1, failedCallBackMethod);
	}
	/// <summary>
	/// 下載資源
	/// </summary>
	/// <param name='requestURL'>
	/// 下載的網址
	/// </param>
	/// <param name='storeName'>
	/// 下載的儲存名稱
	/// </param>
	/// <param name='callBackObject'>
	/// 下載完畢時回Call的物件
	/// </param>
	/// <param name='callBackMethod'>
	/// 下載完畢時回Call的方法名稱
	/// </param>
	public void method_DownloadObject(string requestURL, string storeName, GameObject callBackObject, string callBackMethod, int version, string failedCallBackMethod = "")
	{
		if (string.IsNullOrEmpty(requestURL) || string.IsNullOrEmpty(storeName))
		{
		#if UNITY_EDITOR
			Debug.LogError ("錯誤!，請勿填入空字串");
		#endif
			return;
		}
		
		if (dic_DownloadData.ContainsKey(storeName))
		{
			if (dic_DownloadData[storeName].isDone)
			{
				callBackObject.SendMessage (callBackMethod, storeName, SendMessageOptions.DontRequireReceiver);
				return;
			}
		}
		
		for (int i = 0; i < list_Request.Count; i++)
		{
			if (list_Request[i].RequestURL == requestURL)
			{
			#if UNITY_EDITOR
				Debug.LogWarning("在此前已有相同URL，故不重覆下載");
			#endif
				return;
			}
			if (list_Request[i].StoreName == storeName)
			{
			#if UNITY_EDITOR
				Debug.LogWarning("重複的儲存名稱，請重新命名");
			#endif
				return;
			}
		}
		
		RequestFormat tempFormat		= new RequestFormat();
		tempFormat.RequestURL			= requestURL;
		tempFormat.StoreName			= storeName;
		tempFormat.CallBackObject		= callBackObject;
		tempFormat.CallBackMethod		= callBackMethod;
		tempFormat.Version				= version;
		tempFormat.FailedCallBackMethod	= failedCallBackMethod;
		
		list_Request.Add(tempFormat);
		list_WaitForCallBack.Add(tempFormat);
		
		this.enabled = true;
		//開始下載
		if (!dic_DownloadData.ContainsKey(storeName))
		{
		#if UNITY_EDITOR
			Debug.Log ("開始下載" + storeName);
		#endif
			
			if (version == -1)
				dic_DownloadData.Add(storeName, new WWW(requestURL));
			else
				StartCoroutine (WaitForCache(tempFormat));
		}
	}
	//下載失敗後，重新下載用
	public void method_RedownloadObject(RequestFormat request)
	{
		if (request.Version == -1)
			method_DownloadObject(request.RequestURL, request.StoreName, request.CallBackObject, request.CallBackMethod, request.FailedCallBackMethod);
		else
			method_DownloadObject(request.RequestURL, request.StoreName, request.CallBackObject, request.CallBackMethod, request.Version, request.FailedCallBackMethod);
	}
	
	IEnumerator WaitForCache(RequestFormat format)
	{
		while (!Caching.ready) yield return WaitForEOF;
		
		WWW www = WWW.LoadFromCacheOrDownload (format.RequestURL, format.Version);
		dic_DownloadData.Add(format.StoreName, www);
	}
	
	/// <summary>
	/// 取得下載的%數
	/// </summary>
	/// <returns>
	/// 目前下載進度
	/// </returns>
	/// <param name='ResourceName'>
	/// 資源的名稱
	/// </param>
	public float method_GetProgress(string ResourceName)
	{
		if (dic_DownloadData.ContainsKey(ResourceName))
		{
			//索引值裡面的WWW資料如果還沒下載完成
			if (!dic_DownloadData[ResourceName].isDone)	return dic_DownloadData[ResourceName].progress;
			else 										return 1;
		}
		
	#if UNITY_EDITOR
		Debug.LogWarning ("找不到下載對應名稱" + ResourceName);
	#endif
		return 0;
	}
	
	/// <summary>
	/// 從WWW中生成遊戲物件
	/// </summary>
	/// <returns>
	/// 生成的物件
	/// </returns>
	/// <param name='ResourceName'>
	/// 資源的名稱
	/// </param>
	public GameObject method_GenerateFromWWWBundle(string ResourceName)
	{
		if (dic_DownloadData.ContainsKey(ResourceName))
		{
			if (dic_DownloadData[ResourceName].isDone)
			{
				if (dic_DownloadData[ResourceName].error == null)
					return GameObject.Instantiate(dic_DownloadData[ResourceName].assetBundle.mainAsset) as GameObject;
				else
					return null;
			}
			
		#if UNITY_EDITOR
			Debug.LogWarning("檔案 '" + ResourceName + "' 正在下載中: " + (dic_DownloadData[ResourceName].progress * 100) + "%");
		#endif
			return null;
		}
		
	#if UNITY_EDITOR
		Debug.LogWarning("找不到下載對應名稱 : " + ResourceName);
	#endif
		return null;
	}
	/// <summary>
	/// 從WWW中生成遊戲物件
	/// </summary>
	/// <returns>
	/// 生成的物件
	/// </returns>
	/// <param name='ResourceName'>
	/// 資源的名稱
	/// </param>
	public GameObject method_GenerateFromWWWBundle(string ResourceName, Vector3 GeneratePos)
	{
		if (dic_DownloadData.ContainsKey(ResourceName))
		{
			if (dic_DownloadData[ResourceName].isDone)
				return GameObject.Instantiate(dic_DownloadData[ResourceName].assetBundle.mainAsset, GeneratePos, Quaternion.identity) as GameObject;
			
		#if UNITY_EDITOR
			Debug.LogWarning("檔案 '" + ResourceName + "' 正在下載中: " + (dic_DownloadData[ResourceName].progress * 100) + "%");
		#endif
			return null;
		}
		
	#if UNITY_EDITOR
		Debug.LogWarning("找不到下載對應名稱 : " + ResourceName);
	#endif
		return null;
	}
	
	/// <summary>
	/// 取得WWW資料
	/// </summary>
	/// <returns>
	/// WWW資料
	/// </returns>
	/// <param name='ResourceName'>
	/// 資源的名稱
	/// </param>
	public WWW method_GetWWW(string ResourceName)
	{
		if (dic_DownloadData.ContainsKey(ResourceName))
		{
			if (dic_DownloadData[ResourceName].isDone)
				return dic_DownloadData[ResourceName];
			
		#if UNITY_EDITOR
			Debug.LogWarning("檔案 '" + ResourceName + "' 正在下載中: " + (dic_DownloadData[ResourceName].progress * 100) + "%");
		#endif
			return null;
		}
		
	#if UNITY_EDITOR
		Debug.LogWarning("找不到下載對應名稱 : " + ResourceName);
	#endif
		return null;
	}
	/// <summary>
	/// 卸載資源
	/// </summary>
	/// <param name='ResourceName'>
	/// 資源的名稱
	/// </param>
	public void method_UnloadBundle(string ResourceName, bool unloadAllLoadedObjects)
	{
		if (!dic_DownloadData.ContainsKey(ResourceName)) return;
		
		if (dic_DownloadData[ResourceName] != null)
		{
			if (dic_DownloadData[ResourceName].isDone)
				dic_DownloadData[ResourceName].assetBundle.Unload(unloadAllLoadedObjects);
		}
		dic_DownloadData[ResourceName] = null;
		dic_DownloadData.Remove(ResourceName);
	}
	public void method_UnloadWWW(string ResourceName)
	{
		if (!dic_DownloadData.ContainsKey(ResourceName)) return;
		
		dic_DownloadData[ResourceName] = null;
		dic_DownloadData.Remove(ResourceName);
	}
	
	//手動移除請求 (經由URL)
	public void method_RemoveRequestByURL(string RequestURL)
	{
		if (string.IsNullOrEmpty(RequestURL))
		{
		#if UNITY_EDITOR
			Debug.LogError ("錯誤!，請勿填入空字串");
		#endif
			return;
		}
		
		for (int i = 0; i < list_Request.Count; i++)
		{
			if (list_Request[i].RequestURL == RequestURL)
			{
				list_Request.RemoveAt(i);
				return;
			}
		}
	}
	//手動移除請求 (經由需求名稱)
	public void method_RemoveRequestByName(string RequestName)
	{
		if (string.IsNullOrEmpty(RequestName))
		{
		#if UNITY_EDITOR
			Debug.LogError ("錯誤!，請勿填入空字串");
		#endif
			return;
		}
		
		for (int i = 0; i < list_Request.Count; i++)
		{
			if (list_Request[i].StoreName == RequestName) { list_Request.RemoveAt(i); return; }
		}
	}
	
	public void update_HandleLoadingRequest()
	{
		if (Time.frameCount % 3 == 0)
		{
			//沒有等待CallBack的表單
			if (list_WaitForCallBack.Count == 0)
			{
				this.enabled = false;
				return;
			}
			
			foreach (RequestFormat requtst in list_WaitForCallBack)
			{
				if (dic_DownloadData.ContainsKey(requtst.StoreName))
				{
					if (dic_DownloadData[requtst.StoreName].isDone)
					{
						if (dic_DownloadData[requtst.StoreName].error == null)
						{
							//回傳下載成功的WWW檔案名稱
						#if UNITY_EDITOR
							Debug.Log ("檔案名稱 '" + requtst.StoreName + "' 下載完畢");
						#endif
							requtst.CallBackObject.SendMessage (requtst.CallBackMethod, requtst.StoreName, SendMessageOptions.DontRequireReceiver);
							list_WaitForCallBack.Remove (requtst);
							return;
						}
						else
						{
							if (dic_DownloadData[requtst.StoreName].error.ToString ().Contains ("404"))
							{
								Debug.Log ("檔案遺失");
								//清空有儲存過的資源
								list_WaitForCallBack.Remove (requtst);
								list_Request.Remove (requtst);
								dic_DownloadData.Remove(requtst.StoreName);
								return;
							}
							Debug.Log(dic_DownloadData[requtst.StoreName].error.ToString ());
						#if UNITY_EDITOR
							Debug.LogWarning ("檔案名稱 '" + requtst.StoreName + "' 下載失敗，重新下載檔案");
						#endif
							
							RequestFormat tempFormat		= new RequestFormat();
							tempFormat.RequestURL			= requtst.RequestURL;
							tempFormat.StoreName			= requtst.StoreName;
							tempFormat.CallBackObject		= requtst.CallBackObject;
							tempFormat.CallBackMethod		= requtst.CallBackMethod;
							tempFormat.Version				= requtst.Version;
							tempFormat.FailedCallBackMethod	= requtst.FailedCallBackMethod;
							
							//清空有儲存過的資源
							list_WaitForCallBack.Remove (requtst);
							list_Request.Remove (requtst);
							dic_DownloadData.Remove(requtst.StoreName);
							
							//不CallBack，直接重新下載
							if (string.IsNullOrEmpty(tempFormat.FailedCallBackMethod))
								method_RedownloadObject(tempFormat);
							//錯誤CallBack
							else
								tempFormat.CallBackObject.SendMessage (tempFormat.FailedCallBackMethod, tempFormat, SendMessageOptions.DontRequireReceiver);
							return;
						}
					}
					else
					{
					#if UNITY_EDITOR
//						Debug.Log ("檔案名稱 '" + requtst.StoreName + "' : " + (method_GetProgress(requtst.StoreName) * 100) + "%");
					#endif
					}
				}
			}
		}
	}
	
	public virtual void Awake () { if (gInstance == null) gInstance = this; }
	public virtual void Update () { update_HandleLoadingRequest(); }
}

