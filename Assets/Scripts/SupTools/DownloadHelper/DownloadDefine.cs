﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DownloadDefine
{
}


// 通知介面定義
public interface IDownloadComplete
{
	//所有下載完成 (廣播形式)
	void OnAllDownloadComplete();
	void OnDownloadComplete(DownloadFormat data);
}

// 下載%通知介面定義
public interface IDownloadProgress
{
	void OnTotalProgress(float progress);
	void OnDownloadComplete(DownloadFormat data);
}

//下載列表格式定義
public class DownloadFormat : IClearStruct
{
	public string					Key;
	public int						FileSize;			//2,147,483,647
	public string					FileName;
	public string					FileStorePath;
	public DownloadType				DownloadType;
	public DownloadHandleMethod 	HandleMethod;
	public string					DownloadURL;
	public string					Value;

	public string					Flag;				//自己用來判斷用的標記，隨意設
	public Object					UnpackObj;			//暫存用

	public WWW						Resource;
	public DonwloadStatus			Status;
	public IDownloadComplete		CompleteCallBack;
	public DownloadErrorInfo		ErrorInfo;

	public System.Action<DownloadFormat> OnDownloadComplete;
	public System.Action<DownloadFormat> OnProgressHandle;

	float _DownloadProgress;
	public float DownloadProgress
	{
		get { return _DownloadProgress; }
		set
		{
			_DownloadProgress = value;

			if (OnProgressHandle != null)
				OnProgressHandle(this);
		}
	}

	public bool GotResourceSuccess
	{
		get
		{
			return (Resource != null && Status == DonwloadStatus.Success);
		}
	}

	public DownloadFormat() { Status = DonwloadStatus.Idle; }
	public DownloadFormat(string key, IDownloadComplete caller)
	{
		Key = key;
		CompleteCallBack = caller;
		Status = DonwloadStatus.Idle;
	}
	public DownloadFormat(string key, WWW resource)
	{
		Key = key;
		Status = DonwloadStatus.Success;
		Resource = resource;
	}

	public DownloadFormat(IDownloadComplete caller, string key, string downloadURL, string fileStorePath, int fileSize, DownloadType downloadType, DownloadHandleMethod handleMethod, System.Action<DownloadFormat> onDownloadComplete)
	{
		SetData(caller, key, downloadURL, fileStorePath, fileSize, downloadType, handleMethod, onDownloadComplete);
	}

	public void SetData(IDownloadComplete caller, string key, string downloadURL, string fileStorePath, int fileSize, DownloadType downloadType, DownloadHandleMethod handleMethod, System.Action<DownloadFormat> onDownloadComplete)
	{
		CompleteCallBack	= caller;
		Key					= key;
		DownloadURL			= downloadURL;
		FileStorePath		= fileStorePath;
		FileSize			= fileSize;
		DownloadType		= downloadType;
		HandleMethod		= handleMethod;
		Status				= DonwloadStatus.Idle;
		OnDownloadComplete	= onDownloadComplete;
	}

	public void SetErrorInfo(DownloadErrorType errorType, string errorMsg = "")
	{
		DownloadProgress = 0;
		Status = DonwloadStatus.HasError;

		if (ErrorInfo == null)
			ErrorInfo = new DownloadErrorInfo();
		
		ErrorInfo.ErrorType = errorType;
		ErrorInfo.ErrorMsg = errorMsg;
	}
	
	public void SetComplete()
	{
		DownloadProgress = 1;
		Status = DonwloadStatus.Success;

		if (OnDownloadComplete != null)
			OnDownloadComplete(this);
	}

	public int ResourceFileSize
	{
		get
		{
			if (Resource != null)
			{
				return Resource.bytes.Length;
			}
			
			if (!string.IsNullOrEmpty(FileStorePath))
				return BatchDownloadToolMethod.GetFileSize(FileStorePath);

			return -1;
		}
	}

	public void UnloadResource(bool unload)
	{
		if (Resource != null)
		{
			if (Resource.assetBundle != null)
				Resource.assetBundle.Unload(unload);
		}
	}

	public void Clear()
	{
		if (UnpackObj != null)
			GameObject.DestroyImmediate(UnpackObj, true);

		UnloadResource(true);

		if (Resource != null)
			Resource = null;
	}
}

public enum DonwloadStatus
{
	Idle,
	Downloading,
	HasError,
	Success,
	NeedNotToDownload,
}

public class DonwloadStatusInfo
{
	public string				UrlOrPath;
	public DonwloadStatus		Status;
	public DownloadErrorInfo	ErrorInfo;
	public List<string>			FilterKey;

	public DonwloadStatusInfo(string urlOrPath)
	{
		UrlOrPath = urlOrPath;
		Status = DonwloadStatus.Idle;
	}
	
	public DonwloadStatusInfo(string urlOrPath, string[] filterKey)
	{
		UrlOrPath = urlOrPath;
		Status = DonwloadStatus.Idle;
		FilterKey = GetClassList<string>(filterKey);
	}

	public void SetErrorInfo(DownloadErrorType errorType, string errorMsg = "")
	{
		if (ErrorInfo == null)
			ErrorInfo = new DownloadErrorInfo();

		ErrorInfo.ErrorType = errorType;
		ErrorInfo.UrlOrPath = UrlOrPath;
		ErrorInfo.ErrorMsg = errorMsg;
	}

	//轉換成指定Type的List
	List<T> GetClassList<T>(params object[] values) where T : class
	{
		List<T> result = null;

		if (values != null)
		{
			int length = values.Length;

			if (length > 0)
			{
				result = new List<T>();

				for (int i = 0; i < length; i++)
				{
					result.Add((T)values[i]);
				}
			}
		}
		return result;
	}
}

public enum DownloadErrorType
{
	NoError,
	LocalFileNotExist,
	NoNetwork,
	DownloadFailure,
	FileStorePathNull,
	WhiteInError,
	NoFileDataList,
}

public class DownloadErrorInfo
{
	public DownloadErrorType	ErrorType;
	public string				UrlOrPath;
	public string				ErrorMsg;
	
	public DownloadErrorInfo() {}
	public DownloadErrorInfo(DownloadErrorType errorType, string urlOrPath, string errorMsg = "")
	{
		ErrorType = errorType;
		UrlOrPath = urlOrPath;
		ErrorMsg = errorMsg;
	}

	public override string ToString()
	{
		return Method.GetLogValueFormat("ErrorType", ErrorType, "URLorPath", UrlOrPath, "ErrorMsg", ErrorMsg);
	}
}

public struct DownloadResult
{
    public string TaskName;				//下載的名稱
    public bool TaskResult;				//下載的結果
    
    public DownloadResult(string name, bool taskResult)
	{
        TaskName	= name;
        TaskResult	= taskResult;
    }
}

// 下載的類別
public enum DownloadType
{
	DoNotDownload	=	-1,
	Txt				=	0,
	Assetbundle		=	1,
	WWWObject		=	2,
	ValueOnly		=	3,
}
// 下載的處理方法
public enum DownloadHandleMethod
{
	CacheOnly		= 0,
	StoreOnly		= 1,
	CacheAndStore	= 2
}
