﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//在時間內滑動數字，並給予回傳值
public class LerpFloatTool : MonoBehaviour
{
	static LerpFloatTool gInstance;
	public static LerpFloatTool ins
	{
		get	{
			if (gInstance == null)
				gInstance = GetInstance<LerpFloatTool>();
			return gInstance;
		}
	}
	
	Dictionary<int, bool> Dic_Stopper = new Dictionary<int, bool>();
	
	WaitForEndOfFrame WaitForEOF = new WaitForEndOfFrame();
	
	public bool StopLerp(int uniqueID)
	{
		if (Dic_Stopper.ContainsKey(uniqueID))
			Dic_Stopper[uniqueID] = true;
		
		return Dic_Stopper.ContainsKey(uniqueID);
	}

	public void DoGetValue(float duration, float fromValue, float toValue, System.Action<float> result)
	{
		StartCoroutine( GetValue(duration, fromValue, toValue, result) );
	}

	//yield return StartCoroutine( LerpFloatTool.ins.GetValue(1, 1, 0, value => XXXX = value) );
	public IEnumerator GetValue(float duration, float fromValue, float toValue, System.Action<float> result)
	{
		float timeCounter	= 0;
		float rValue		= fromValue;
		float deltaValue	= (toValue - fromValue) / duration;
		
		while (timeCounter < duration)
		{
			timeCounter += Time.deltaTime;
			rValue += deltaValue * Time.deltaTime;
			
			result(rValue);
			yield return WaitForEOF;
		}
		result(toValue);
	}
	
	public IEnumerator GetValueRealTime(float duration, float fromValue, float toValue, System.Action<float> result)
	{
		float timeCounter	= 0;
		float rValue		= fromValue;
		float deltaValue	= (toValue - fromValue) / duration;
		
		while (timeCounter < duration)
		{
			timeCounter += Time.unscaledDeltaTime;
			rValue += deltaValue * Time.unscaledDeltaTime;
			
			result(rValue);
			yield return WaitForEOF;
		}
		result(toValue);
	}
	
	//yield return StartCoroutine( LerpFloatTool.ins.GetValue(1, 1, 0, value => XXXX = value, 1234) );
	public IEnumerator GetValue(float duration, float fromValue, float toValue, System.Action<float> result, int uniqueID)
	{
		//有正在執行的就停止它
		if (Dic_Stopper.ContainsKey(uniqueID))
		{
			Dic_Stopper[uniqueID] = true;
			yield return WaitForEOF;
			Dic_Stopper[uniqueID] = false;
		}
		else
		{
			Dic_Stopper.Add(uniqueID, false);
		}
		
		float timeCounter	= 0;
		float rValue		= fromValue;
		float deltaValue	= (toValue - fromValue) / duration;
		
		while (timeCounter < duration && !Dic_Stopper[uniqueID])
		{
			timeCounter += Time.deltaTime;
			rValue += deltaValue * Time.deltaTime;
			
			result(rValue);
			yield return WaitForEOF;
		}
		
		if (Dic_Stopper[uniqueID] == false)	//非強制停止
		{
			result(toValue);
			Dic_Stopper.Remove(uniqueID);
		}
		else
		{
//			Debug.Log ("STOP : " + uniqueID);
		}
	}
	
	public IEnumerator GetValueRealTime(float duration, float fromValue, float toValue, System.Action<float> result, int uniqueID)
	{
		//有正在執行的就停止它
		if (Dic_Stopper.ContainsKey(uniqueID))
		{
			Dic_Stopper[uniqueID] = true;
			yield return WaitForEOF;
			Dic_Stopper[uniqueID] = false;
		}
		else
		{
			Dic_Stopper.Add(uniqueID, false);
		}
		
		float timeCounter	= 0;
		float rValue		= fromValue;
		float deltaValue	= (toValue - fromValue) / duration;
		
		while (timeCounter < duration && !Dic_Stopper[uniqueID])
		{
			timeCounter += Time.unscaledDeltaTime;
			rValue += deltaValue * Time.unscaledDeltaTime;
			
			result(rValue);
			yield return WaitForEOF;
		}
		
		if (Dic_Stopper[uniqueID] == false)	//非強制停止
		{
			result(toValue);
			Dic_Stopper.Remove(uniqueID);
		}
		else
		{
//			Debug.Log ("STOP : " + uniqueID);
		}
	}

#region 其他函式

	static T GetInstance<T>(bool autoGenerateNew = true) where T : UnityEngine.Component
	{
		T gInstance = GameObject.FindObjectOfType(typeof(T)) as T;

		if (gInstance == null)
		{
			string AddToSceneObjectName = typeof(T).GetType().Name;
			GameObject target = GameObject.Find(AddToSceneObjectName);

			//只允許在Runtime的時候生成新的
			if (autoGenerateNew && Application.isPlaying)
			{
				if (target == null) target = new GameObject(AddToSceneObjectName);
				gInstance = target.AddComponent<T>();
			}
		}
		return gInstance;
	}

	T GetComponent<T>(GameObject targetObject) where T : UnityEngine.Component
	{
		if (targetObject == null) return null;
		T script = targetObject.GetComponent<T>();
		if (script) return script;
		script = targetObject.AddComponent<T>();

		return script;
	}

#endregion
}
