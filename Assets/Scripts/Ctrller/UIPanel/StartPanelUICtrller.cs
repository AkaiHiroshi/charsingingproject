﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class StartPanelUICtrller : PanelUICtrllerBase
{
	public Text SongName;
	public Text CVName;
	public Text Info;

	public void SetData(LoadDataInfo.StartPanelInfo data)
	{
		SongName.text = data.SongName;
		CVName.text = data.CVName;
		Info.text = data.Info;
	}
}
