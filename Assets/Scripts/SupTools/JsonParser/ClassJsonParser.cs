﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;

public class ClassJsonParser : ClassJsonParserMethodBase
{
	static char Symbol = '"';

	public static string ConvertClassToJson<T>(T data) where T : class
	{
		string result = "{";

		FieldInfo[] allInfo = GetAllFieldInfo(data);

		for (int i = 0, max = allInfo.Length; i < max; i++)
		{
			result = CombineString(result, HandleConvertFieldInfo(allInfo[i], data));

			if (i == max-1)	//最後一個參數
			{
				result = RemoveLastCommon(result);
			}
		}

		result = CombineString(result, "}");
		return result;
	}

	static string HandleConvertFieldInfo<T>(FieldInfo info, T data) where T : class
	{
		string result = "";
		object infoValue = info.GetValue(data);

		if (infoValue != null)
		{
			if (infoValue is IList)
			{
				IList list = infoValue as IList;
				
				if (list != null)
				{
					System.Type listType = GetListElementType(info);

					if (listType == null)
						listType = GetArrayElementType(info);
					
					if (listType != null)
					{
						string tempString = "";
						string tempResult = "";

						string eleName = (listType != null) ? listType.Name : "";

//						Debug.Log(eleName);

						if (listType.IsEnum ||
							eleName == "Boolean" ||
							eleName == "String"
						)
						{
							foreach (var element in list)
							{
								tempResult = CombineString(tempResult, Symbol, element, Symbol, ",");
							}
							tempResult = RemoveLastCommon(tempResult);

//							result = CombineString(result, Symbol, info.Name, Symbol, ": ", Symbol, infoValue, Symbol, ",");
						}
						else if (eleName == "Vector2")
						{
							try 
							{
								Vector2 v2 = Vector2.zero;

								foreach (var element in list)
								{
									v2 = (Vector2)element;
									tempResult = CombineString(tempResult, Symbol, v2.x, ",", v2.y, Symbol, ",");
								}
								tempResult = RemoveLastCommon(tempResult);
							}
							catch (System.Exception) {}
						}
						else if (eleName == "Vector3")
						{
							try 
							{
								Vector3 v3 = Vector3.zero;

								foreach (var element in list)
								{
									v3 = (Vector3)element;
									tempResult = CombineString(tempResult, Symbol, v3.x, ",", v3.y, ",", v3.z, Symbol, ",");
								}
								tempResult = RemoveLastCommon(tempResult);
							}
							catch (System.Exception) {}
						}
						else if (eleName == "Color")
						{
							try 
							{
								Color c = Color.white;

								foreach (var element in list)
								{
									c = (Color)element;
									tempResult = CombineString(tempResult, Symbol, c.r, ",", c.g, ",", c.b, ",", c.a, Symbol, ",");
								}
								tempResult = RemoveLastCommon(tempResult);
							}
							catch (System.Exception) {}
						}
						else if (eleName == "Color32")
						{
							try 
							{
								Color32 c32;

								foreach (var element in list)
								{
									c32 = (Color32)element;
									tempResult = CombineString(tempResult, Symbol, c32.r, ",", c32.g, ",", c32.b, ",", c32.a, Symbol, ",");
								}
								tempResult = RemoveLastCommon(tempResult);
							}
							catch (System.Exception) {}
						}
						else if (listType is IDictionary ||
							eleName == "Texture2D" ||
							eleName == "Font" ||
							eleName == "GameObject" ||
							eleName == "Transform" ||
							eleName == "MonoBehaviour"
						)
						{
//							Debug.LogError("無法轉換之類別：" + info.Name);
						}
						else
						{
							if (listType.IsClass)
							{
								foreach (var element in list)
								{
									tempString = ConvertClassType(element);

									if (!string.IsNullOrEmpty(tempString))
									{
										tempResult = CombineString(tempResult, tempString);
									}
								}
							}
							else
							{
								foreach (var element in list)
								{
									tempResult = CombineString(tempResult, element, ",");
								}
								tempResult = RemoveLastCommon(tempResult);
							}
						}

						if (!IsNullString(tempResult))
						{
							result = CombineString(result, Symbol, info.Name, Symbol, ": [ ", tempResult, " ],");
						}
					}
				}
			}
			else if (info.FieldType.IsEnum ||
			        infoValue is bool ||
			        infoValue is byte ||
			        infoValue is int ||
			        infoValue is float ||
			        infoValue is double ||
			        infoValue is string
			        )
			{
				result = CombineString(result, Symbol, info.Name, Symbol, ": ", Symbol, infoValue, Symbol, ",");
			}
			else if (infoValue is Vector2 ||
			         infoValue is Vector3)
			{
				result = CombineString(result, Symbol, info.Name, Symbol, ": ", Symbol, infoValue.ToString().Replace("(", "").Replace(")", ""), Symbol, ",");
			}
			else if (infoValue is Color32 || infoValue is Color)
			{
				result = CombineString(result, Symbol, info.Name, Symbol, ": ", Symbol, infoValue.ToString().Replace("RGBA(", "").Replace(")", ""), Symbol, ",");
			}
			else if (infoValue is IDictionary ||
			         infoValue is Texture2D ||
			         infoValue is Font ||
			         infoValue is GameObject ||
			         infoValue is Transform ||
			         infoValue is MonoBehaviour)
			{
//				Debug.LogError("無法轉換之類別：" + info.Name);
			}
			else
			{
				string tempString = ConvertClassType(infoValue);
				
				if (!string.IsNullOrEmpty(tempString))
				{
					result = CombineString(result, Symbol, info.Name, Symbol, ": ", tempString);
				}
			}
		}
		return result;
	}

	static string NullStringResult;
	static bool IsNullString(string tempString)
	{
		NullStringResult = tempString.Replace(" ", "");
		NullStringResult = NullStringResult.Replace(",", "");
		NullStringResult = NullStringResult.Replace("{", "");
		NullStringResult = NullStringResult.Replace("}", "");

		return string.IsNullOrEmpty(NullStringResult);
	}

	static string ConvertClassType(object infoValue)
	{
		bool append = true;
		string result = "";
		
		try
		{
			result = CombineString(result, "{ ");
			FieldInfo[] allInfo = GetAllFieldInfo(infoValue);
			
			for (int i = 0, max = allInfo.Length; i < max; i++)
			{
//				if (allInfo[i].FieldType.ToString() == "UIPanel")
//				{
//					Debug.Log(allInfo[i].Name + "/" + allInfo[i].FieldType.Name + "/" + allInfo[i].FieldType.FullName + "/" + allInfo[i].FieldType.Assembly.GetName().Name);
//				}
				result = CombineString(result, HandleConvertFieldInfo(allInfo[i], infoValue));
			}
			result = RemoveLastCommon(result);
			result = CombineString(result, " },");
		}
		catch (System.Exception) { append = false; }

		return ((append) ? result : "");
	}
	
	static string RemoveLastCommon(string str)
	{
		if (str.EndsWith(","))
		{
			return str.Substring(0, str.Length-1);
		}
		return str;
	}



	public static void ParseText<T>(TextAsset docAsset, ref T data) where T : class
	{
		if (docAsset == null) return;
		ParseText<T>(docAsset.text, ref data);
	}

	public static void ParseText<T>(WWW jsonWWW, ref T data) where T : class
	{
		if (string.IsNullOrEmpty(jsonWWW.error))
		{
			bool transJsonFailure = false;
			string text = "";
			Dictionary<string, object> dic = null;

			try
			{
				text = jsonWWW.text;
				dic = Json.Deserialize(text) as Dictionary<string, object>;
				transJsonFailure = (dic == null);
			}
			catch (System.Exception)
			{
				transJsonFailure = true;
				text = "";
			}

			if (transJsonFailure)
			{
				try
				{
					text = System.Text.Encoding.UTF8.GetString(jsonWWW.bytes, 3, jsonWWW.bytes.Length - 3);  // Skip thr first 3 bytes (i.e. the UTF8 BOM)
					dic = Json.Deserialize(text) as Dictionary<string, object>;
					transJsonFailure = (dic == null);
				}
				catch (System.Exception)
				{
					transJsonFailure = true;
					text = "";
				}
			}

			if (transJsonFailure)	Debug.LogError("轉換Json失敗");
			else					ParseText<T>(text, ref data);
		}
	}

	/// <summary>
	/// 自動Maping Json參數到你要的Class上
	/// </summary>
	/// <param name="json">Json字串</param>
	/// <param name="data">你要的Class主體</param>
	/// <typeparam name="T">The 1st type parameter.</typeparam>
	public static void ParseText<T>(string jsonText, ref T data) where T : class
	{
		if (!string.IsNullOrEmpty(jsonText))
		{
			bool transJsonFailure = false;
			Dictionary<string, object> dic = null;

			try
			{
				dic = Json.Deserialize(jsonText) as Dictionary<string, object>;
				transJsonFailure = (dic == null);
			}
			catch (System.Exception)
			{
				transJsonFailure = true;
			}

			if (transJsonFailure)
				Debug.LogError(CombineString("轉換Json失敗：\n", jsonText));
			else
				HandleParseDicType(dic, ref data);
		}
	}

	static void HandleParseDicType<T>(Dictionary<string, object> dic, ref T data) where T : class
	{
		if (dic == null) return;
		if (data == null) { Debug.LogError("DATA = NULL"); return; }

		foreach (var item in dic)
		{
//			Debug.LogWarning(item.Key + " / " + item.Value);

			if (item.Value is IList)
			{
				FieldInfo listInfo = GetFieldInfo(data, item.Key);

				if (listInfo != null)
				{
					int handleType = -1;
					System.Type elementType = null;

					if (IsArrayType(listInfo))
					{
						handleType = 0;
						elementType = GetArrayElementType(listInfo);
					}
					else if (IsListType(listInfo))
					{
						handleType = 1;
						elementType = GetListElementType(listInfo);
					}

					if (handleType >= 0)
					{
						List<object> list = item.Value as List<object>;

						if (list != null)
						{
							List<object> toList = new List<object>();

							for (int i = 0, count = list.Count; i < count; i++)
							{
								if (list[i] == null) continue;

								if (list[i] is IDictionary)
								{
//									Debug.LogError("IS DICTIONARY : " + item.Key + "/ handleType = " + handleType + "/ Value["+i+"] = " + list[i]);

									Dictionary<string, object> dic1 = list[i] as Dictionary<string, object>;

									if (dic1 != null && elementType != null)
									{
//										try
										{
											object classObj = CreateNewInstance(elementType);

											if (classObj != null)
												HandleParseDicType(dic1, ref classObj);

											toList.Add(classObj);
										}
//										catch (System.Exception ex)
//										{
//											/* 若有發生像這問題 System.MissingMethodException: Method not found: 'Default constructor not found...ctor() of ItemInfo'. at System.Activator.CreateInstance
//											 * 要檢查自己的class上，有無中誇號裡的那一段 (無任何值的宣告) =>  public class ItemInfo {「public ItemInfo() {}」 } */
//											Debug.LogError(ex);
//										}
									}
								}
								else if (list[i] is IList)
								{
									Debug.LogError("未寫解析ILIST形式：" + item.Key);
								}
								else
								{
//									Debug.LogError("ISNT DICTIONARY : " + item.Key + "/ elementType = " + elementType + "/ Value["+i+"] = " + list[i]);

									toList.Add(list[i]);
								}
							}
							
							if (toList.Count > 0)
							{
								if (handleType == 0) 		//ArrayType
								{
									System.Array newArray = CreateNewArrayInstance(elementType, toList);

									try { listInfo.SetValue(data, newArray); }
									catch (System.Exception ex) { Debug.LogError(ex); }
								}
								else if (handleType == 1)	//ListType
								{
									IList newList = CreateNewListInstance(elementType, toList);

									try { listInfo.SetValue(data, newList); }
									catch (System.Exception ex) { Debug.LogError(ex); }
								}
							}
							toList.Clear();
						}
					}
				}
			}
			else if (item.Value is IDictionary)
			{
				FieldInfo dicInfo = GetFieldInfo(data, item.Key);

				if (dicInfo != null)
				{
					if (dicInfo.FieldType.IsClass)
					{
						Dictionary<string, object> dic1 = item.Value as Dictionary<string, object>;

						if (dic1 != null)
						{
							object classObj = GetFieldValue(data, item.Key);

							try
							{
								if (classObj == null)
									classObj = CreateNewInstance(dicInfo.FieldType);

								if (classObj != null)
								{
									HandleParseDicType(dic1, ref classObj);
									SetFieldValue(data, item.Key, classObj);
								}
							}
							catch (System.Exception ex)
							{
								/* 若有發生像這問題 System.MissingMethodException: Method not found: 'Default constructor not found...ctor() of ItemInfo'. at System.Activator.CreateInstance
								 * 要檢查自己的class上，有無中誇號裡的那一段 (無任何值的宣告) =>  public class ItemInfo {「public ItemInfo() {}」 } */
								Debug.LogError(item.Key + " : " + ex);
							}
						}
					}
				}
			}
			else
			{
				if (item.Value != null)
				{
					if (!string.IsNullOrEmpty(item.Value.ToString()))
					{
						SetFieldValue(data, item.Key, item.Value);
					}
				}
			}
		}
	}
}
