﻿using UnityEngine;
using System.Collections;

public class SimpleFadeTool : MonoBehaviour
{
	public enum FadeType
	{
		FadeIn 		= 1,
		FadeOut 	= 2,
	}
	public enum FadeState
	{
		idle		= 0,
		FadeIn 		= 1,
		FadeOut 	= 2,
		process		= 4,
	}

	public FadeState CurrentState = FadeState.idle;
	public static FadeState CurrentFadeState = FadeState.idle;

	static float FadeDuration;
	static Color FadeColor;
	
	static float alpha;
	public static float Alpha
	{
		get { return alpha;  }
		set	{ alpha = Mathf.Clamp01(value); }
	}
	
	public static bool IsFilled
	{
		get { return (Alpha >= 1); }
	}

	public Texture2D FadeInOutTexture;
	public Color GUI_Color = new Color(0, 0, 0, 0);
	
	int Sign;
	float OriFadeDuration;
	Rect TextureRect;
	
#region 公用函式

	//淡入淡出函式
	public static void FadeIn() { Fade(FadeType.FadeIn, 1); }
	public static void FadeIn(float EffectDuration) { Fade(FadeType.FadeIn, EffectDuration); }
	public static void FadeIn(float EffectDuration, Color fadeColor) { Fade(FadeType.FadeIn, EffectDuration, fadeColor); }
	
	public static void FadeOut() { Fade(FadeType.FadeOut, 1); }
	public static void FadeOut(float EffectDuration) { Fade(FadeType.FadeOut, EffectDuration); }
	public static void FadeOut(float EffectDuration, Color fadeColor) { Fade(FadeType.FadeOut, EffectDuration, fadeColor); }
	
	public static void Fade(FadeType targetType, float EffectDuration) { Fade(targetType, EffectDuration, Color.black); }
	public static void Fade(FadeType targetType, float EffectDuration, Color fadeColor)
	{
//		Debug.Log("CALL FADE : " + targetType);

		if ((targetType == FadeType.FadeIn  && Alpha == 0) || 
			(targetType == FadeType.FadeOut && Alpha == 1))
			return;
		
		CheckScriptExists();
		
		FadeColor		 = fadeColor;
		CurrentFadeState = StringToEnum<FadeState>(targetType.ToString());
		FadeDuration 	 = EffectDuration;
	}
	
	//填滿
	public static void FillColor() { FillColor(Color.black); }
	public static void FillColor(Color fillColor)
	{
		CheckScriptExists();
		
		Alpha = 1;
		FadeDuration = 0;
		FadeColor = fillColor;
		CurrentFadeState = FadeState.idle;
	}
#endregion
	
#region 私有方法
	static SimpleFadeTool script;
	static void CheckScriptExists()
	{
		if (script == null)
		{
			script = GameObject.FindObjectOfType(typeof(SimpleFadeTool)) as SimpleFadeTool;
			if (script == null)
				script = new GameObject("FadeInOutBehaviour").AddComponent<SimpleFadeTool>();
		}
	}

	static T StringToEnum<T>(string enumString) where T : struct
	{
		if(!typeof(T).IsEnum)
		{
			throw new System.ArgumentException("丟入Type必須為列舉Type");
		}
		else
		{
			try
			{
				T result = (T)System.Enum.Parse(typeof(T), enumString);
				return result;
			}
			catch (System.Exception ex)
			{
				throw ex;
			}
		}
	}
#endregion
	
#region UNITY預設函式
	void Awake()
	{
		if (script == null)
			script = this;
		
		if (FadeInOutTexture == null)
		{
			FadeInOutTexture = new Texture2D(1,1);
			FadeInOutTexture.SetPixel(0,0,Color.white);
			FadeInOutTexture.Apply();
		}
		
		TextureRect = new Rect(0, 0, Screen.width+10, Screen.height+10);
	}
	
	void Update()
	{
		switch (CurrentFadeState)
		{
		case FadeState.idle :
			GUI_Color = FadeColor;
			break;
		case FadeState.FadeIn :
			GUI_Color = FadeColor;
			Alpha = 1;
			Sign = -1;
			OriFadeDuration = FadeDuration;
			CurrentFadeState = FadeState.process;
			break;
		case FadeState.FadeOut :
			GUI_Color = new Color(FadeColor.r, FadeColor.g, FadeColor.b, 0);
			Alpha = 0;
			Sign = 1;
			OriFadeDuration = FadeDuration;
			CurrentFadeState = FadeState.process;
			break;
		case FadeState.process :
			GUI_Color.a = Alpha;
			if (FadeDuration > 0)
			{
				Alpha += Sign * Time.deltaTime / OriFadeDuration;
				FadeDuration -= Time.deltaTime;
			}
			else
			{
				FadeDuration = 0;
				CurrentFadeState = FadeState.idle;
			}
			break;
		}

		CurrentState = CurrentFadeState;
	}
	
	void OnGUI()
	{	
		if (alpha <= 0) return;
		GUI.depth = 0;
		GUI.color = GUI_Color;
		GUI.DrawTexture(TextureRect, FadeInOutTexture);
	}
#endregion
	
#region Setup
	#if UNITY_EDITOR
	[UnityEditor.MenuItem ("My Tools/設置/在場景中設置FadeInOutBehaviour")]
	static void SetMyScript()
	{
		CheckScriptExists();
		GameObject ScriptPanel = GameObject.Find("_Script");
		if (ScriptPanel != null)
			script.transform.parent = ScriptPanel.transform;
	}
	#endif
#endregion
}
