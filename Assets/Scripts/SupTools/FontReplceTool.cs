﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class FontReplceTool : MonoBehaviour
{
	public Font FontFile;
	public Text[] Targets;

	[ContextMenu("FindAllSceneText")]
	void FindAllSceneText()
	{
		Targets = FindObjectsOfType<Text>();
	}

	[ContextMenu("Replace")]
	void Replace()
	{
		if (Targets == null || FontFile == null) return;

		for (int i = 0, max = Targets.Length; i < max; i++)
		{
			if (Targets[i] != null)
				Targets[i].font = FontFile;
		}
	}

	void Awake()
	{
		Replace();
	}
}
