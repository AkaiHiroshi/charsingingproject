﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LyricsDataDefine {}

[System.Serializable]
public class LyricsCharDataInfo
{
	public string Character;
	[HideInInspector]
	public string CharacterWithoutSpace;
	public float PlayDuration = 0.8f;
	public float Time;

	//繋（つな） -> [Character = つ; CorrOriWord = 繋; CorrOriWordIndex = 0] [Character = な; CorrOriWord = 繋; CorrOriWordIndex = 1]
	public string CorrOriWord;
	public int CorrOriWordIndex = -1;

	public LyricsCharDataInfo(string character, string playDuration = "")
	{
		Character = character;
		CharacterWithoutSpace = Character.Replace(" ", "").Replace("　", "");

		if (!string.IsNullOrEmpty(playDuration))
			Method.ParseValueToFloat(playDuration, out PlayDuration);
	}
}

[System.Serializable]
public class LyricsDataInfo : IClearStruct
{
	public float Sec;
	public string OriLyrics;
	public string LangLyrics;
	public string BracketsRemoveLyrics;
	public string BracketsReplaceLyrics;
	public string TimeString;
	public string DurationString;
	public List<LyricsCharDataInfo> CharLyricsData = new List<LyricsCharDataInfo>();

	public LyricsDataInfo(string timeString, string lyrics, string langLyrics, string playDuration)
	{
		Sec = DataFormat.ConvertTimeTextToSec(timeString);
		if (Sec < 0) return;

		OriLyrics = lyrics;
		LangLyrics = langLyrics;
		DurationString = playDuration;
		BracketsRemoveLyrics = OriLyrics;

		while (BracketsRemoveLyrics.Contains("(") && BracketsRemoveLyrics.Contains(")"))		//半形
		{
			BracketsRemoveLyrics = BracketsRemoveLyrics.Replace("(" + Method.GetBetweenString(BracketsRemoveLyrics, '(', ')') + ")", "");
		}

		while (BracketsRemoveLyrics.Contains("（") && BracketsRemoveLyrics.Contains("）"))		//全形
		{
			BracketsRemoveLyrics = BracketsRemoveLyrics.Replace("（" + Method.GetBetweenString(BracketsRemoveLyrics, '（', '）') + "）", "");
		}
	}

	public void UpdateDocumentString()
	{
		TimeString = Method.CombineString("[", DataFormat.GetTimeText(Sec), "]");
		DurationString = "";

		float time = Sec;

		if (CharLyricsData != null)
		{
			for (int i = 0, max = CharLyricsData.Count; i < max; i++)
			{
				CharLyricsData[i].Time = time;
				time += CharLyricsData[i].PlayDuration;
				DurationString += CharLyricsData[i].PlayDuration.ToString() + ",";
			}
		}

		if (DurationString.Length > 0)
			DurationString = DurationString.Remove(DurationString.Length-1, 1);
	}

	public void UpdateBracketsReplaceLyrics(List<string> wordDataKeys)
	{
		Method.ClearClassList(ref CharLyricsData);

		string result = OriLyrics;

		//handle Chinese character to Japanese character
		{
			result = ReplaceChineseWord(result, '(', ')', wordDataKeys);
			result = ReplaceChineseWord(result, '（', '）', wordDataKeys);
			BracketsReplaceLyrics = result;
		}

		//split single char data
		{
			int index = 0;
			string tempString = "";
			List<char> words = new List<char>();
			Method.AddArrayToList<char>(BracketsReplaceLyrics.ToCharArray(), ref words);
			string[] durationSplit = DurationString.Split(',');

			while (words.Count > 0)
			{
				index = 0;
				tempString = words[0].ToString();

				if (wordDataKeys.Contains(tempString))
				{
					while (index+1 < words.Count)	//CheckNexChar
					{
						if (wordDataKeys.Contains(words[index+1].ToString())) 
						{
							break;
						}
						else
						{
							tempString += words[index+1].ToString();
							index++;
						}
					}

					//					Debug.LogError(tempString);

					if (durationSplit.Length > CharLyricsData.Count)
						CharLyricsData.Add( new LyricsCharDataInfo(tempString, durationSplit[CharLyricsData.Count]) );
					else
						CharLyricsData.Add( new LyricsCharDataInfo(tempString) );

					for (int i = 0, max = tempString.Length; i < max; i++)
					{
						words.RemoveAt(0);
					}
				}
				else
				{
					Debug.LogError("Char Error : " + tempString);
					break;
				}
			}
		}

		//sort replaced Chinese character to CharLyricsData
		{
			bool open = false;
			int cldIndex = 0;
			int tempIndex = 0;
			int removeCount = 0;
			int oriWordIndex = 0;
			string tempString = "";
			string curOriWord = "";
			List<char> words = new List<char>();
			Method.AddArrayToList<char>(OriLyrics.ToCharArray(), ref words);
			//			Debug.LogWarning(OriLyrics);

			while (words.Count > 0)
			{
				removeCount = 1;
				tempString = words[0].ToString();

				if (open)
				{
					tempIndex = 0;
					tempString = words[0].ToString();

					if (DataFormat.IsBracketsCharacter(tempString))
					{
						switch (tempString)
						{
						case "(" :
						case "（" :
							break;

						case "）" :
						case ")" :
							oriWordIndex = 0;
							curOriWord = "";
							open = false;
							//							Debug.LogWarning("-------------- CLOSE");
							break;
						}
					}
					else
					{
						while (tempIndex+1 < words.Count)		//CheckNexChar
						{
							if (wordDataKeys.Contains(words[tempIndex+1].ToString()) || 
								DataFormat.IsSpaceCharacter(words[tempIndex+1].ToString()) || 
								DataFormat.IsBracketsCharacter(words[tempIndex+1].ToString())
							)
							{
								break;
							}
							else
							{
								tempString += words[tempIndex+1].ToString();
								tempIndex++;
							}
						}

//						Debug.Log("ADD OPENED : " + Method.GetLogValueFormat("tempString", tempString, "cldIndex", cldIndex, "curOriWord", curOriWord));
						CharLyricsData[cldIndex].CorrOriWord = curOriWord;
						CharLyricsData[cldIndex].CorrOriWordIndex = oriWordIndex;

						cldIndex++;
						oriWordIndex++;
					}

					removeCount = tempString.Length;
				}
				else
				{
					if (wordDataKeys.Contains(tempString))
					{
						//						Debug.LogError( Method.GetLogValueFormat("words", words[0], "cldIndex", cldIndex) );

						tempIndex = 0;
						while (tempIndex+1 < words.Count)		//CheckNexChar
						{
							if (wordDataKeys.Contains(words[tempIndex+1].ToString()) || DataFormat.IsSpaceCharacter(words[tempIndex+1].ToString())) 
							{
								break;
							}
							else
							{
								tempString += words[tempIndex+1].ToString();
								tempIndex++;
							}
						}

						if (wordDataKeys.Contains(tempString)) 
						{
							cldIndex++;
							removeCount = tempString.Length;
						}
						else
						{
							cldIndex++;
						}
					}
					else 			//Chinese word
					{
						if (!DataFormat.IsSpaceCharacter(tempString))		//Skip SPACE
						{
							curOriWord = tempString;
							open = true;
							//							Debug.LogWarning("-------------- OPEN FROM => " + tempString);
						}
						else
						{
							curOriWord = "";
						}
					}
				}

				//				Debug.Log("---------> removeCount = " + removeCount);

				if (removeCount == 1)
				{
					words.RemoveAt(0);
				}
				else
				{
					for (int j = 0; j < removeCount; j++) words.RemoveAt(0);
				}
			}
		}
	}

	string ReplaceChineseWord(string checkString, char checkChar1, char checkChar2, List<string> wordDataKeys)
	{
		string result = checkString;
		string replaceFrom = "";
		string replaceTo = "";
		string tempString = "";
		string lastString = "";
		int lastStart = 0;
		List<string> stringList = new List<string>();

		while (checkString.Contains(checkChar1.ToString()) && checkString.Contains(checkChar2.ToString()))
		{
			FilterChineseWord(checkString, checkChar1, checkChar2, wordDataKeys, ref replaceFrom, ref replaceTo, ref lastStart);

			tempString = checkString.Substring(0, lastStart+1);
			//			Debug.Log(Method.GetLogValueFormat("tempString", tempString, "checkString", checkString, "lastStart", lastStart));
			checkString = checkString.Substring(lastStart+1, checkString.Length-lastStart-1);
			tempString = tempString.Replace(replaceFrom, "").Replace(checkChar1.ToString(), "").Replace(checkChar2.ToString(), "");
			lastString = checkString;

			stringList.Add(tempString);
		}

		if (stringList.Count > 0)
		{
			result = "";
			for (int i = 0, max = stringList.Count; i < max; i++)
			{
				result += stringList[0];
				stringList.RemoveAt(0);
			}

			return result + lastString;
		}
		else
		{
			return result;
		}
	}
	void FilterChineseWord(string checkString, char checkChar1, char checkChar2, List<string> wordDataKeys, ref string replaceFrom, ref string replaceTo, ref int lastStart)
	{
		int fromI	= checkString.IndexOf(checkChar1)+1;
		//		int fromI	= checkString.IndexOf(checkChar1)+1;
		lastStart	= checkString.IndexOf(checkChar2);

		if (fromI < 0 || lastStart < 0)
		{
			replaceFrom = "";
			replaceTo = "";
		}
		else
		{
			replaceTo = checkString.Substring(fromI, lastStart-fromI);
			replaceTo = replaceTo.Replace(checkChar1.ToString(), "");
			replaceTo = replaceTo.Replace(checkChar2.ToString(), "");

			int removeTo = fromI-2;
			int frontEnd = removeTo;
			char[] words = checkString.ToCharArray();
			for (; frontEnd >= 0; frontEnd--)
			{
				if (DataFormat.IsSpaceCharacter(words[frontEnd].ToString()) || wordDataKeys.Contains(words[frontEnd].ToString()))
				{
					break;
				}

				replaceFrom = words[frontEnd].ToString();

			}

			frontEnd++;
		}
	}

	public void Clear()
	{
		Method.ClearClassList(ref CharLyricsData);
	}
}

