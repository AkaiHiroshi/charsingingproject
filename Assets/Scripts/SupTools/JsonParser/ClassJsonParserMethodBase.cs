﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

public abstract class ClassJsonParserMethodBase 
{
	static Color DefaultColor = new Color(1, 1, 1, 1);
	static Color32 DefaultColor32 = new Color32(255, 255, 255, 255);
	const BindingFlags Flags = BindingFlags.Public | BindingFlags.Instance | BindingFlags.Static;

	/// <summary>
	/// 實體化  Ex：效力等同於 MyClass c = 「new MyClass()」;
	/// </summary>
	/// <returns>The new instance</returns>
	/// <param name="type">Type.</param>
	protected static object CreateNewInstance(System.Type type)
	{
		if (type != null) return System.Activator.CreateInstance(type);
		return null;
	}

	protected static System.Array CreateNewArrayInstance(System.Type elementType, List<object> values)
	{
		if (values == null || elementType == null) return null;

		int arrCount = values.Count;
		System.Array result = System.Array.CreateInstance(elementType, arrCount);

		try
		{
			for (int i = 0; i < arrCount; i++)
			{
				result.SetValue(ConvertValueToRightTypeValue(values[i], elementType), i);
			}
		} 
		catch (System.Exception ex) 
		{
			Debug.LogError(elementType + " / elementType = " + elementType + "\n" + ex);
		}
		return result;
	}

	protected static System.Array CreateNewArrayInstance(System.Type elementType, params object[] values)
	{
		if (values == null || elementType == null) return null;

		int arrCount = values.Length;
		System.Array result = System.Array.CreateInstance(elementType, arrCount);

		try
		{
			for (int i = 0; i < arrCount; i++)
			{
				if (values[i] != null)
					result.SetValue(ConvertValueToRightTypeValue(values[i], elementType), i);
			}
		} 
		catch (System.Exception ex) 
		{
			Debug.LogError(elementType + " / elementType = " + elementType + "\n" + ex);
		}
		return result;
	}

	protected static IList CreateNewListInstance(System.Type elementType, List<object> values)
	{
		if (values == null || elementType == null) return null;

		var listType = typeof(List<>);
		var constructedListType = listType.MakeGenericType(elementType);
		IList result = (IList)System.Activator.CreateInstance(constructedListType);

//		try
		{
			for (int i = 0, arrCount = values.Count; i < arrCount; i++)
			{
				if (values[i] != null)
					result.Add(ConvertValueToRightTypeValue(values[i], elementType));
			}
		} 
//		catch (System.Exception ex)
//		{
//			Debug.LogError(constructedListType + " / elementType = " + elementType + "\n" + ex);
//		}
		return result;
	}

	protected static IList CreateNewListInstance(System.Type elementType, params object[] values)
	{
		if (values == null || elementType == null) return null;

		var listType = typeof(List<>);
		var constructedListType = listType.MakeGenericType(elementType);
		IList result = (IList)System.Activator.CreateInstance(constructedListType);

		try
		{
			for (int i = 0, arrCount = values.Length; i < arrCount; i++)
			{
				if (values[i] != null)
					result.Add( ConvertValueToRightTypeValue(values[i], elementType) );
			}
		} 
		catch (System.Exception ex) 
		{
			Debug.LogError(constructedListType + " / elementType = " + elementType + "\n" + ex);
		}
		return result;
	}
	
	protected static bool IsArrayType(FieldInfo info)
	{
		return (info.FieldType.IsArray && info.FieldType.FullName.EndsWith("[]"));
	}
	protected static bool IsListType(FieldInfo info)
	{
		return (info.FieldType.IsGenericType && info.FieldType.Name.Contains("List"));
	}

	protected static object ConvertValueToRightTypeValue(object value, System.Type elementType = null)
	{
		object r = null;
		string eleName = (elementType != null) ? elementType.Name : "";

		try
		{
			if (eleName == "Boolean")
			{
				bool result = false;
				if (ParseValueToBool(value, out result))
					r = result;
			}
			else if (eleName == "Byte")
			{
				byte result = 0;
				if (ParseValueToByte(value, out result))
					r = result;
			}
			else if (eleName == "Int32")
			{
				int result = 0;
				if (ParseValueToInt(value, out result))
					r = result;
			}
			else if (eleName == "Double")
			{
				double result = 0;
				if (ParseValueToDouble(value, out result))
					r = result;
			}
			//Unity元件
			else if (eleName == "Vector2")
			{
				Vector2 result = Vector2.zero;
				if (ParseValueToVector2(value, out result))
					r = result;
			}
			else if (eleName == "Vector3")
			{
				Vector3 result = Vector3.zero;
				if (ParseValueToVector3(value, out result))
					r = result;
			}
			else if (eleName == "Color")
			{
				Color result;
				if (ParseValueToColor(value, DefaultColor, out result))
				{
					r = result;
				}
			}
			else if (eleName == "Color32")
			{
				Color32 result;
				if (ParseValueToColor32(value, DefaultColor32, out result))
				{
					r = result;
				}
			}
			else
			{
				r = value;
			}
		}
		catch (System.Exception) {}

		return r;
	}

	/// <summary>
	/// 取得Array的元素類型  Ex：'int[]'就回傳'int'  'MyClass[]'就回傳'MyClass'
	/// </summary>
	/// <returns>Element type</returns>
	/// <param name="fieldInfo">Field info</param>
	protected static System.Type GetArrayElementType(FieldInfo fieldInfo)
	{
		if (IsArrayType(fieldInfo))
		{
			try
			{
				return System.Type.GetType(string.Format("{0},{1}", fieldInfo.FieldType.FullName.Substring(0, fieldInfo.FieldType.FullName.Length - 2), fieldInfo.FieldType.Assembly.GetName().Name));
			}
			catch (System.Exception) {}
		}
		Debug.LogError("GetArrayElementType Failure");
		return null;
	}

	/// <summary>
	/// 取得List的元素類型  Ex：'List<int>'就回傳'int'  'List<MyClass>'就回傳'MyClass'
	/// </summary>
	/// <returns>Element type</returns>
	/// <param name="fieldInfo">Field info</param>
	protected static System.Type GetListElementType(FieldInfo fieldInfo)
	{
		if (IsListType(fieldInfo))
		{
			try
			{
				string elementName = GetBetweenString(fieldInfo.FieldType.ToString(), '[', ']');
				System.Type result = System.Type.GetType(elementName);

				if (result == null)
				{
					var assemblyName = elementName.Substring( 0, elementName.IndexOf( '.' ) );

					var assembly = Assembly.Load( assemblyName );
					if (assembly != null)
						result = assembly.GetType( elementName );
				}
//				Debug.Log(result);
				return result;
			}
			catch (System.Exception) {}
		}
//		Debug.LogError("GetListElementType Failure");
		return null;
	}

//	protected static System.Type GetListElementType(IList list)
//	{
//		if (list != null)
//		{
//			try
//			{
//				string elementName = GetBetweenString(list.GetType().ToString(), '[', ']');
//				System.Type result = System.Type.GetType(elementName);
//
//				if (result == null)
//				{
//					var assemblyName = elementName.Substring( 0, elementName.IndexOf( '.' ) );
//
//					var assembly = Assembly.Load( assemblyName );
//					if (assembly != null)
//						result = assembly.GetType( elementName );
//				}
////				Debug.Log(result);
//				return result;
//			}
//			catch (System.Exception) {}
//		}
//		Debug.LogError("GetListElementType Failure : " + list);
//		return null;
//	}

	/// <summary>
	/// 嘗試把參數轉換為陣列型態
	/// </summary>
	/// <returns>陣列型態的Value</returns>
	/// <param name="fieldInfo">Field info</param>
	/// <param name="value">Value</param>
	protected static object ConvertValueToEnumValue(FieldInfo fieldInfo, object value)
	{
		if (fieldInfo.FieldType.IsEnum)
		{
			try
			{
				return System.Enum.Parse(fieldInfo.FieldType, value.ToString());
			}
			catch (System.Exception) {}
		}
		return null;
	}

	/// <summary>
	/// 取得特定Class中的指定名稱欄位
	/// </summary>
	/// <returns>Field info</returns>
	/// <param name="targetClass">Target class</param>
	/// <param name="fieldName">欄位名稱</param>
	/// <typeparam name="T">Class Type</typeparam>
	protected static FieldInfo GetFieldInfo<T>(T targetClass, string fieldName) where T : class
	{
		return targetClass.GetType().GetField(fieldName);
	}

	/// <summary>
	/// 取得特定Class中的指定名稱欄位的數值
	/// </summary>
	/// <returns>Field value</returns>
	/// <param name="targetClass">Target class</param>
	/// <param name="fieldName">欄位名稱</param>
	/// <typeparam name="T">Class Type</typeparam>
	protected static object GetFieldValue<T>(T targetClass, string fieldName) where T : class
	{
		FieldInfo fieldInfo = targetClass.GetType().GetField(fieldName);
		return (fieldInfo != null) ? fieldInfo.GetValue(targetClass) : null;
	}

	/// <summary>
	/// Set特定Class中的指定名稱欄位數值
	/// </summary>
	/// <param name="targetClass">Target class</param>
	/// <param name="fieldName">欄位名稱</param>
	/// <param name="value">要Set的數值</param>
	/// <typeparam name="T">Class Type</typeparam>
	protected static void SetFieldValue<T>(T targetClass, string fieldName, object value) where T : class
	{
		FieldInfo fieldInfo = targetClass.GetType().GetField(fieldName);

		if (fieldInfo != null)
		{
			try
			{
				object infoValue = fieldInfo.GetValue(targetClass);

				if (fieldInfo.FieldType.IsEnum)
				{
					object result = ConvertValueToEnumValue(fieldInfo, value);
					if (result != null)
						fieldInfo.SetValue(targetClass, result);
				}
				else if (infoValue is bool)
				{
					bool result = false;
					if (ParseValueToBool(value, out result))
						fieldInfo.SetValue(targetClass, result);
				}
				else if (infoValue is byte)
				{
					byte result = 0;
					if (ParseValueToByte(value, out result))
						fieldInfo.SetValue(targetClass, result);
				}
				else if (infoValue is int)
				{
					int result = 0;
					if (ParseValueToInt(value, out result))
						fieldInfo.SetValue(targetClass, result);
				}
				else if (infoValue is float)
				{
					float result = 0;
					if (ParseValueToFloat(value, out result))
						fieldInfo.SetValue(targetClass, result);
				}
				else if (infoValue is double)
				{
					double result = 0;
					if (ParseValueToDouble(value, out result))
						fieldInfo.SetValue(targetClass, result);
				}
				else if (infoValue is Vector2)
				{
					Vector2 result = Vector2.zero;
					if (ParseValueToVector2(value, out result))
						fieldInfo.SetValue(targetClass, result);
				}
				else if (infoValue is Vector3)
				{
					Vector3 result = Vector3.zero;
					if (ParseValueToVector3(value, out result))
						fieldInfo.SetValue(targetClass, result);
				}
				else if (infoValue is Color)
				{
					Color result;
					if (ParseValueToColor(value, DefaultColor, out result))
					{
						fieldInfo.SetValue(targetClass, result);
					}
				}
				else if (infoValue is Color32)
				{
					Color32 result;
					if (ParseValueToColor32(value, DefaultColor32, out result))
					{
						fieldInfo.SetValue(targetClass, result);
					}
				}
				else
				{
					fieldInfo.SetValue(targetClass, value);
				}
			}
			catch (System.Exception) {}
		}
	}

	protected static FieldInfo[] GetAllFieldInfo<T>(T targetClass) where T : class
	{
		//		FieldInfo[] result = targetClass.GetType().GetFields(Flags);
		//		foreach (FieldInfo fieldInfo in result) { Debug.Log("Field=" + fieldInfo.Name + "Value = " + fieldInfo.GetValue(targetClass)); }
		return targetClass.GetType().GetFields(Flags);
	}

	protected static PropertyInfo[] GetAllPropertyInfo<T>(T targetClass) where T : class
	{
		//		PropertyInfo[] result = targetClass.GetType().GetProperties(Flags);
		//		foreach (PropertyInfo propertyInfo in result) { Debug.Log("Field=" + propertyInfo.Name); }
		return targetClass.GetType().GetProperties(Flags);
	}


	protected static object InvokeMethod<T>(T targetClass, string methodName, System.Object[] param) where T : class
	{
		MethodInfo methodInfo = targetClass.GetType().GetMethod(methodName);
		return (methodInfo != null) ? methodInfo.Invoke(targetClass, param) : null;
	}

	protected static string GetBetweenString(string checkString, char checkChar1, char checkChar2)
	{
		int fromI	= checkString.IndexOf(checkChar1)+1;
		int toI		= checkString.IndexOf(checkChar2);

		if (fromI < 0)	fromI	= 0;
		if (toI < 0)	toI		= checkString.Length-1;

		return checkString.Substring(fromI, toI-fromI);
	}

	protected static bool ParseValueToBool(object obj, out bool outValue)
	{
		bool result = false;
		bool success = false;
		string strValue = obj.ToString();

		if (!string.IsNullOrEmpty(strValue))
		{
			success = bool.TryParse(strValue, out result);
		}
		else
		{
			success = true;
		}
		outValue = result;
		return success;
	}

	protected static bool ParseValueToByte(object obj, out byte outValue)
	{
		byte result = 0;
		string strValue = obj.ToString();

		bool success = false;

		if (!string.IsNullOrEmpty(strValue))
		{
			success = byte.TryParse(strValue, out result);
		}
		else
		{
			success = true;
		}
		outValue = result;
		return success;
	}

	protected static bool ParseValueToInt(object obj, out int outValue)
	{
		int result = 0;
		string strValue = obj.ToString();

		bool success = false;

		if (!string.IsNullOrEmpty(strValue))
		{
			success = int.TryParse(strValue, out result);
		}
		else
		{
			success = true;
		}
		outValue = result;
		return success;
	}

	protected static bool ParseValueToFloat(object obj, out float outValue)
	{
		float result = 0;
		string strValue = obj.ToString();

		bool success = false;

		if (!string.IsNullOrEmpty(strValue))
		{
			success = float.TryParse(strValue, out result);
		}
		else
		{
			success = true;
		}
		outValue = result;
		return success;
	}

	protected static bool ParseValueToDouble(object obj, out double outValue)
	{
		double result = 0;
		string strValue = obj.ToString();

		bool success = false;

		if (!string.IsNullOrEmpty(strValue))
		{
			success = double.TryParse(strValue, out result);
		}
		else
		{
			success = true;
		}
		outValue = result;
		return success;
	}

	protected static bool ParseValueToVector2(object obj, out Vector2 outValue)
	{
		Vector2 result = Vector2.zero;
		string strValue = obj.ToString();

		bool success = false;

		if (!string.IsNullOrEmpty(strValue))
		{
			bool interrupt = false;
			string[] split = strValue.Split(',');
			List<float> values = new List<float>();

			for (int i = 0, max = split.Length; i < max; i++)
			{
				float tValue = 0;

				if (float.TryParse(split[i], out tValue))
				{
					values.Add(tValue);
				}
				else
				{
					interrupt = true;
					break;
				}
			}

			if (!interrupt)
			{
				if (values.Count >= 2)
				{
					success = true;
					result = new Vector2(values[0], values[1]);
				}
			}
			values.Clear();
		}
		else
		{
			success = true;
		}
		outValue = result;
		return success;
	}

	protected static bool ParseValueToVector3(object obj, out Vector3 outValue)
	{
		Vector3 result = Vector3.zero;
		string strValue = obj.ToString();

		bool success = false;

		if (!string.IsNullOrEmpty(strValue))
		{
			bool interrupt = false;
			string[] split = strValue.Split(',');
			List<float> values = new List<float>();

			for (int i = 0, max = split.Length; i < max; i++)
			{
				float tValue = 0;

				if (float.TryParse(split[i], out tValue))
				{
					values.Add(tValue);
				}
				else
				{
					interrupt = true;
					break;
				}
			}

			if (!interrupt)
			{
				if (values.Count >= 3)
				{
					success = true;
					result = new Vector3(values[0], values[1], values[2]);
				}
			}
			values.Clear();
		}
		else
		{
			success = true;
		}
		outValue = result;
		return success;
	}

	protected static bool ParseValueToColor(object objValue, Color defaultColor, out Color outValue)
	{
		string strValue = objValue.ToString();

		Color result = defaultColor;
		bool success = false;

		if (!string.IsNullOrEmpty(strValue))
		{
			bool interrupt = false;
			string[] split = strValue.Split(',');
			List<float> values = new List<float>();

			for (int i = 0, max = split.Length; i < max; i++)
			{
				float tValue = 0;

				if (float.TryParse(split[i], out tValue))
				{
					values.Add(tValue);
				}
				else
				{
					interrupt = true;
					break;
				}
			}

			if (!interrupt)
			{
				if (values.Count == 3)
				{
					success = true;
					result = new Color(values[0], values[1], values[2], 1);
				}
				else if (values.Count >= 4)
				{
					success = true;
					result = new Color(values[0], values[1], values[2], values[3]);
				}
			}
			values.Clear();
		}
		else
		{
			success = true;
		}
		outValue = result;
		return success;
	}

	protected static bool ParseValueToColor32(object obj, Color32 defaultColor, out Color32 outValue)
	{
		Color32 result = defaultColor;
		string strValue = obj.ToString();

		bool success = false;

		if (!string.IsNullOrEmpty(strValue))
		{
			bool interrupt = false;
			string[] split = strValue.Split(',');
			List<byte> values = new List<byte>();

			for (int i = 0, max = split.Length; i < max; i++)
			{
				byte tValue = 0;

				if (byte.TryParse(split[i], out tValue))
				{
					values.Add(tValue);
				}
				else
				{
					interrupt = true;
					break;
				}
			}

			if (!interrupt)
			{
				if (values.Count == 3)
				{
					success = true;
					result = new Color32(values[0], values[1], values[2], 255);
				}
				else if (values.Count >= 4)
				{
					success = true;
					result = new Color32(values[0], values[1], values[2], values[3]);
				}
			}
			values.Clear();
		}
		else
		{
			success = true;
		}
		outValue = result;
		return success;
	}
	
	protected static StringBuilder SB;
	protected static string CombineString(params object[] values)
	{
		string result = "";
		
		if (SB == null)
			SB = new StringBuilder();
		
		for (int i = 0; i < values.Length; i++)
		{
			SB.Append(values[i].ToString());
		}
		
		result = SB.ToString();
		SB.Length = 0;	//清空
		
		return result;
	}

}
