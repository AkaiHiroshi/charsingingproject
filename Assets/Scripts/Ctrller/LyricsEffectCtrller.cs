﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LyricsEffectCtrller : MonoBehaviour
{
	public int FontSize = 24;
	public float DefaultLength;
	public RectTransform UIRect;

	protected Vector2 UISize;
	protected Vector2 FromToSize;

	public float Width
	{
		get { return UIRect.sizeDelta.x; }
		set
		{
			UISize.x = value;
			UISize.y = UIRect.sizeDelta.y;
			UIRect.sizeDelta = UISize;
		}
	}


	public virtual void ShowLerpCutOffEffect(string fromLyrics, string toLyrics, float duration)
	{
		if (UIRect == null) return;

		UISize.y = UIRect.sizeDelta.y;
		FromToSize.x = DefaultLength + TextFileTool.ins.GetTextWidth(FontSize, fromLyrics);		//from width
		FromToSize.y = DefaultLength + TextFileTool.ins.GetTextWidth(FontSize, toLyrics);		//to width
//		Debug.Log(Method.GetLogValueFormat("fromLyrics", fromLyrics, "toLyrics", toLyrics, "from width", FromToSize.x, "to width", FromToSize.y));

		if (Application.isPlaying && duration > 0)
		{
			StopAllCoroutines();
			StartCoroutine( LerpFloatTool.ins.GetValue(duration, FromToSize.x, FromToSize.y, value =>
				{
					Width = value;
				}) );
		}
		else
		{
			UISize.x = FromToSize.y;
			UIRect.sizeDelta = UISize;
		}
	}

	protected virtual void Awake()
	{
		if (UIRect == null)
			UIRect = GetComponent<RectTransform>();
	}
}
