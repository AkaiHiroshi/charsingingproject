﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//這個外掛可以幫把Json的文件直接Mapping數值到你的自定義Class上
public class JsonParserExample : MonoBehaviour
{
	public TextAsset JsonTextAsset;
	public string JsonString;

	public MyFormat Data;

	void Start()
	{
		ClassJsonParser.ParseText(JsonTextAsset.text, ref Data);
	}

	void OnGUI()
	{
		GUI.Label(new Rect(30, 30, Screen.width*0.7f, 500), Data.ToString());
		
		if (GUI.Button(new Rect(30, 150, 250, 50), "把Class轉換為JsonString"))
		{
			JsonString = ClassJsonParser.ConvertClassToJson(Data);
		}


		if (!string.IsNullOrEmpty(JsonString))
		{
			GUI.Label(new Rect(30, 250, Screen.width*0.7f, 500), JsonString);
		}
	}




	[System.Serializable]
	public class MyFormat
	{
		public string Name;
		public string Intro;
		public MyEnum TestEnum = MyEnum.Idle;
		public bool TF;

		public List<bool> NormalList0;
		public List<int> NormalList1;
		public List<string> NormalList2;
		public List<double> NormalList3;
		public List<Color32> NormalList4;
		public List<Color> NormalList5;
		public List<byte> NormalList6;
		public List<Vector2> NormalList7;
		public List<Vector3> NormalList8;

		public Class1 TestClass;
		public Class2[] TestClassArray;
		public List<Class2> TestClassList = new List<Class2>();


		public enum MyEnum
		{
			Idle,
			Atk,
			Run
		}

		[System.Serializable]
		public class Class1
		{
			public string MyParam1;
			public int MyParam2;
			public double MyParam3;
			public Vector2 MyParam4;
			public Vector3 MyParam5;
			public Color32 MyParam6;
			
			public override string ToString()
			{
				return "MyParam1 = " + MyParam1 + "/MyParam2 = " + MyParam2 + "/MyParam3 = " + MyParam3;
			}
		}

		public enum TextType
		{
			Text =	0,
			Label =	1,
		}

		[System.Serializable]
		public class Class2
		{
			public bool Enable;											//bool
			public TextType Type = TextType.Text;						//Enum
			public int FontSize = 16;									//int
			public float Depth;											//float
			public string Info;											//string
			public Vector2 Pos = Vector2.zero;							//vector2
			public Vector3 Scale = Vector3.zero;						//vector3
			public Color32 Color = new Color32(255, 255, 255, 255);		//Color32
		}

	}
}
