﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneDataBase : MonoBehaviour
{
	protected DataManager _Data { get { return DataManager.ins; } }

	protected float TotalTimeCounter						{ get { return DataManager.ins.TotalTimeCounter; } set { DataManager.ins.TotalTimeCounter = value; } }
	protected List<string> WordDataKeys						{ get { return DataManager.ins.WordDataKeys; } }
	protected List<WordDataInfo> WordData					{ get { return DataManager.ins.WordData; } }
	protected List<LyricsDataInfo> LyricsData				{ get { return DataManager.ins.LyricsData; } }
	protected List<ActionCommandDataInfo> ExpressionData	{ get { return DataManager.ins.CommandData; } }


}
