﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class ExpressionCtrller : Live2DModelBase, IUpdateMouth, IUpdatePanValue
{
	public bool EnableRotWithPan = true;
	public float FadeDuration = 2;
	public float MouthLerpDuration = 0.2f;
	public float MouthSmoothValue = 0.5f;
	public float MaxHeadAngle = Live2DMaxHeadAngle;

	public string BrowParamName_R = "PARAM_BROW_R_Y";
	public string BrowParamName_L = "PARAM_BROW_L_Y";
	public string EyesParamName_R = "PARAM_EYE_R_OPEN";
	public string EyesParamName_L = "PARAM_EYE_L_OPEN";
	public string SmileParamName = "PARAM_SMILE";
	public string MouthFormParamName = "PARAM_MOUTH_FORM";
	public string MouthOpenXParamName = "PARAM_MOUTH_OPEN_X";
	public string MouthOpenYParamName = "PARAM_MOUTH_OPEN_Y";
	public string EyeballParamName_X = "PARAM_EYE_BALL_X";
	public string EyeballParamName_Y = "PARAM_EYE_BALL_Y";
	public string HeadAngleParamName_X = "PARAM_ANGLE_X";
	public string HeadAngleParamName_Y = "PARAM_ANGLE_Y";
	public string BodyAngleParamName_X = "PARAM_BODY_ANGLE_X";
	public string BodyAngleParamName_Y = "PARAM_BODY_ANGLE_Y";

	[Range(0, 1)]
	public float BrowMove;
	[Range(0, 1)]
	public float EyesOpen;
	[Range(0, 1)]
	public float Smile;
	[Range(-1, 1)]
	public float MouthForm;
	[Range(0, 1)]
	public float MouthOpen_X;
	[Range(0, 1)]
	public float MouthOpen_Y;
	[Range(-1, 1)]
	public float EyeballAngle_X;
	[Range(-1, 1)]
	public float EyeballAngle_Y;
	[Range(-Live2DMaxHeadAngle, Live2DMaxHeadAngle)]
	public float HeadAngle_X;
	[Range(-Live2DMaxHeadAngle, Live2DMaxHeadAngle)]
	public float HeadAngle_Y;
	[Range(-10, 10)]
	public float BodyAngle_X;
	[Range(-10, 10)]
	public float BodyAngle_Y;

	public string MouthValues;

	bool AllowUpdateMouthByAni = false;
	const float Live2DMaxHeadAngle = 30;

	public void FadeIn()
	{
		SimpleFadeTool.FadeIn(FadeDuration);
	}

	public void FadeOut()
	{
		SimpleFadeTool.FadeOut(FadeDuration);
	}

	public void ShowEndBoard()
	{
		SceneManager.ins.ShowEndBoard();
	}

	public void StartUpdateMouthByAnimation()
	{
		AllowUpdateMouthByAni = true;
	}
	public void StopUpdateMouthByAnimation()
	{
		AllowUpdateMouthByAni = false;
	}

	public void ResetMouth()
	{
		StopAllCoroutines();
		StopUpdateMouthByAnimation();

		StartCoroutine( LerpFloatTool.ins.GetValue(MouthLerpDuration, Live2DModel.getParamFloat(MouthFormParamName), 1, (float value) => { Live2DModel.setParamFloat(MouthFormParamName, value); } ) );
		StartCoroutine( LerpFloatTool.ins.GetValue(MouthLerpDuration, Live2DModel.getParamFloat(MouthOpenXParamName), 1, (float value) => { Live2DModel.setParamFloat(MouthOpenXParamName, value); } ) );
		StartCoroutine( LerpFloatTool.ins.GetValue(MouthLerpDuration, Live2DModel.getParamFloat(MouthOpenYParamName), 0, (float value) => { Live2DModel.setParamFloat(MouthOpenYParamName, value);} ) );
	}

	public void UpdatePanValue(float angleX, float angleY, float maxX, float maxY)
	{
		if (EnableRotWithPan)
		{
			if (angleX > maxX)
				angleX -= 360;
			if (angleY > maxY)
				angleY -= 360;

			float xRatio = angleX/Mathf.Abs(maxX);
			float yRatio = angleY/Mathf.Abs(maxY);

			EyeballAngle_X = -xRatio;
			EyeballAngle_Y = yRatio;
			HeadAngle_X = Mathf.Clamp(xRatio * Live2DMaxHeadAngle, -MaxHeadAngle, MaxHeadAngle);
			HeadAngle_Y = -yRatio * Live2DMaxHeadAngle;
			BodyAngle_X = xRatio * 10;
			BodyAngle_Y = yRatio * 10;

//			Debug.Log(Method.GetLogValueFormat("panX", angleX, "panY", angleY, "maxX", maxX, "maxY", maxY, "xRatio", xRatio, "yRatio", yRatio));
		}
	}

	public void UpdateMouthByWordData(WordDataInfo data, float nextWordIntervalDuration)
	{
		StopAllCoroutines();
		StopUpdateMouthByAnimation();

		if (Live2DModel != null)
		{
			nextWordIntervalDuration *= 0.8f;
			nextWordIntervalDuration = (nextWordIntervalDuration > MouthLerpDuration) ? MouthLerpDuration : nextWordIntervalDuration;

			for (int i = 0, max = data.ValueData.Count; i < max; i++)
			{
				StartCoroutine( UpdateMouthAniByData(data.ValueData[i].Key, data.ValueData[i].WeightValueData, nextWordIntervalDuration, Live2DModel.getParamFloat(data.ValueData[i].Key)) );
			}
		}
	}

	IEnumerator UpdateMouthAniByData(string key, List<WordWeightValueDataInfo> data, float nextWordIntervalDuration, float fromValue)
	{
		float toValue = 0;

		for (int i = 0, max = data.Count; i < max; i++)
		{
			toValue = data[i].Value;

			if (i < max-1)
			{
				toValue = fromValue + (toValue - fromValue) * MouthSmoothValue;
			}

			yield return StartCoroutine( LerpFloatTool.ins.GetValue(nextWordIntervalDuration * data[i].PlayRatio, fromValue, toValue, (float value) =>
			{ 
				Live2DModel.setParamFloat(key, value);
			} ) );
		}
	}

	protected override void UpdateMotionParam()
	{
		//Brow
		Live2DModel.setParamFloat(BrowParamName_R, BrowMove);
		Live2DModel.setParamFloat(BrowParamName_L, BrowMove);

		//Eyes
		Live2DModel.setParamFloat(EyesParamName_R, EyesOpen);
		Live2DModel.setParamFloat(EyesParamName_L, EyesOpen);
		Live2DModel.setParamFloat(SmileParamName, Smile);

		if (!Application.isPlaying || AllowUpdateMouthByAni)
		{
			//Mouth
			Live2DModel.setParamFloat(MouthFormParamName, MouthForm);
			Live2DModel.setParamFloat(MouthOpenXParamName, MouthOpen_X);
			Live2DModel.setParamFloat(MouthOpenYParamName, MouthOpen_Y);
		}

		if (EnableRotWithPan)
		{
			//Eyeball
			Live2DModel.setParamFloat(EyeballParamName_X, EyeballAngle_X);
			Live2DModel.setParamFloat(EyeballParamName_Y, EyeballAngle_Y);

			//Head
			Live2DModel.setParamFloat(HeadAngleParamName_X, HeadAngle_X);
			Live2DModel.setParamFloat(HeadAngleParamName_Y, HeadAngle_Y);

			//Body
			Live2DModel.setParamFloat(BodyAngleParamName_X, BodyAngle_X);
			Live2DModel.setParamFloat(BodyAngleParamName_Y, BodyAngle_Y);
		}
	}

	[ContextMenu("ExportMouthValues")]
	void ExportMouthValues()
	{
		MouthValues = Method.CombineString(MouthForm, "\t", MouthOpen_X, "\t", MouthOpen_Y);
	}

	protected override void Awake()
	{
		base.Awake();

		if (AudioManager.ins)
			AudioManager.ins.RegisterInterface(this);

		MaxHeadAngle = Mathf.Clamp(Mathf.Abs(MaxHeadAngle), 0, Live2DMaxHeadAngle);
	}
}
