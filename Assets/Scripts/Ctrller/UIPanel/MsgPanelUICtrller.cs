﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MsgPanelUICtrller : PanelUICtrllerBase
{
	public Text Info;

	public void SetData(string msg)
	{
		Info.text = msg;
	}
}
