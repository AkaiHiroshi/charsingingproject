﻿using UnityEngine;
using System.Collections;

public class SimpleTwinklingUITextCtrller : SimpleUITextFadeCtrller
{
	public Vector2 TwinklingAlpha = new Vector2(0, 1);
	public float TwinklingDuration = 1;
	public float WaitDuration = 0.2f;

	void OnEnable()
	{
		FadeToOne();
	}

	void OnDisable()
	{
		CancelInvoke();
	}

	void FadeToOne()
	{
		Fade(TwinklingDuration, TwinklingAlpha.x, TwinklingAlpha.y, () => { Invoke("FadeToZero", WaitDuration); });
	}

	void FadeToZero()
	{
		Fade(TwinklingDuration, TwinklingAlpha.y, TwinklingAlpha.x, () => { Invoke("FadeToOne", WaitDuration); });
	}
}
