﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraPanBase : MonoBehaviour
{
	public float Range = 1f;
	public Vector2 Degrees = new Vector2(5f, 3f);
	public Transform TransTarget;

	protected Quaternion StartRotation;
	protected Vector2 TempRot = Vector2.zero;
	protected float delta, halfWidth, halfHeight, x, y;

	protected virtual void Awake()
	{
		if (TransTarget == null)
			TransTarget = transform;
		
		StartRotation = TransTarget.localRotation;
	}

	protected virtual void Update()
	{
//	#if (UNITY_ANDROID || UNITY_IOS || UNITY_IPHONE) && !UNITY_EDITOR

//	#else

		delta = Time.unscaledDeltaTime;
		halfWidth = Screen.width * 0.5f;
		halfHeight = Screen.height * 0.5f;
		if (Range < 0.1f) Range = 0.1f;
		x = Mathf.Clamp((Input.mousePosition.x - halfWidth) / halfWidth / Range, -1f, 1f);
		y = Mathf.Clamp((Input.mousePosition.y - halfHeight) / halfHeight / Range, -1f, 1f);
		TempRot = Vector2.Lerp(TempRot, new Vector2(x, y), delta * 5f);

		TransTarget.localRotation = StartRotation * Quaternion.Euler(-TempRot.y * Degrees.y, TempRot.x * Degrees.x, 0f);
//	#endif
	}
}
